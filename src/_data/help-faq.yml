- question: What is Tripetto?
  answer: Good question! Tripetto offers solutions that let you easily create forms and surveys that are really smart. By <a href="/help/articles/how-to-make-your-forms-smart-and-conversational/" target="_blank">making your forms smart</a> you give your respondents the idea of a conversation instead of a boring form, resulting in higher form completion rates.<br/><br/>The smartness of your forms is the key. To create that smartness you'll need logic. And to create that logic, you'll need a visual form builder that makes your flows visually. And that's where Tripetto makes the difference compared to other form builders. The Tripetto form builder represents your form in a map view, making your logic visually in branches. In that way all flows and decisions inside the form are understandable while building your forms.<br/><br/>Ok, cool, but what really matters is how the form looks, right?! On the front-end we've done our best to let the form feel like a real conversation, respecting the logic that's behind the form. You can choose from 3 different <a href="/help/articles/how-to-switch-between-form-faces/" target="_blank">form faces</a> to meet your purpose.<ul><li>Autoscroll;</li><li>Chat;</li><li>Classic.</li></ul><br/>And you can fully <a href="/help/articles/how-to-style-your-forms/" target="_blank">style your form</a> to your needs and taste.
  areas: [common, studio, wordpress]

- question: What is the difference between the studio and WordPress plugin?
  answer: The base of the studio and WordPress plugin are exactly the same. You can create <a href="/help/articles/how-to-make-your-forms-smart-and-conversational/" target="_blank">smart conversational forms</a>, <a href="/help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">use logic</a>, switch between <a href="/help/articles/how-to-switch-between-form-faces/" target="_blank">form faces (autoscroll, chat, classic)</a>, <a href="/help/articles/how-to-style-your-forms/" target="_blank">style your forms</a>, etc.<br/><br/>The differences come from the moment you're ready to share your form and collect responses.<ul><li>Sharing forms and surveys<br/>From created in the studio can be shared with a <a href="/help/articles/how-to-share-a-link-to-your-form-from-the-studio/" target="_blank">shareable link</a>, or <a href="/help/articles/how-to-embed-a-form-from-the-studio-into-your-website/" target="_blank">embedded</a> anywhere else with the embed code.<br/>Forms created in the WP plugin can be published on the corresponding WordPress site only, using the <a href="/help/articles/how-to-share-a-link-to-your-form-in-your-wordpress-site/" target="_blank">shareable link</a>, or the <a href="/help/articles/how-to-embed-your-form-in-your-wordpress-site/" target="_blank">shortcode</a>.</li><li>Managing data and results<br/>In the studio you can determine how your data, including responses, is saved. At the European infrastructure of Tripetto, or <a href="/help/articles/how-to-take-control-over-your-data-from-the-studio/" target="_blank">take full control over your data</a>.<br/>In the WordPress plugin all your data is saved inside your own WP infrastructure/databases, so absolutely no data gets to any third-parties.</li></ul>
  areas: [common]

- question: What is the studio?
  answer: The studio lets everybody instantly create and share smart forms and surveys that feel like conversations. You can start creating right away at <a href="https://tripetto.app" target="_blank">tripetto.app</a>. When your form is ready to go live, you can share a link, or embed it in your website.<br/><br/>Your responses can be stored at Tripetto, or you can even <a href="/help/articles/how-to-take-control-over-your-data-from-the-studio/" target="_blank">take full control over your data</a>.
  areas: [studio]

- question: What is the WordPress plugin?
  answer: The WordPress plugin lets WordPressers instantly create and publish smart forms into their WP site, using a shareable link or with a shortcode.<br/><br/>The whole plugin runs inside your own WP instance, so no third-party account needed. All data, including responses, are stored inside your own WP databases only.
  areas: [wordpress]

- question: What is the visual form builder?
  answer: Smartness of your forms is key. To create that smartness you'll need logic. And to create that logic, you'll need a visual form builder that makes your flows visually. And that's where Tripetto makes the difference compared to other form builders.<br/><br/>The <a href="/help/articles/learn-the-basics-of-the-form-builder/" target="_blank">Tripetto form builder</a> represents your form in a map view, making your logic visually in branches. In that way all flows and decisions inside the form are understandable while building your forms.
  areas: [common, studio, wordpress]

- question: How do my forms look?
  answer: All forms feel like a real conversation, respecting the logic that's behind the form. You can choose from 3 different <a href="/help/articles/how-to-switch-between-form-faces/" target="_blank">form faces</a> to meet your purpose.<ul><li>Autoscroll;</li><li>Chat;</li><li>Classic.</li></ul><br/>And you can fully <a href="/help/articles/how-to-style-your-forms/" target="_blank">style your form</a> to your needs and taste. You can have a look at <a href="/examples/" target="_blank">our examples</a> for some different styled smart forms.
  areas: [common, wordpress, studio]

- question: Can you show some examples?
  answer: Yes, we love to show some examples! Please have a look at our <a href="/examples/" target="_blank">Examples</a> page. You can use each template as inspiration, or even as a starting point for your own form in the studio.
  areas: [common, wordpress, studio]

- question: What does Tripetto cost?
  answer: It depends on the product you're using. See our <a href="/pricing/" target="_blank">pricing page</a> for more information.
  areas: [common]

- question: How much does it cost to use the studio?
  answer: You can start using the studio for free, even without an account. See our <a href="/pricing/" target="_blank">pricing page</a> for more information.
  areas: [studio]

- question: How much does it cost to use the WordPress plugin?
  answer: You can start using the plugin for free. You even get all <a href="/pricing/" target="_blank">premium features</a> for free for one designated form or survey. See our <a href="/pricing/" target="_blank">pricing page</a> for more information.
  areas: [wordpress]

- question: How can keep I track of updates and other news?
  answer: We'd like to keep you up-to-date on what we're working on. In our <a href="/changelog/" target="_blank">changelog</a> we keep a list of completed releases. Over there, we also have a <a href="/roadmap/" target="_blank">roadmap</a> with an overview of features we're working on, or planned to work on in the near future.
  areas: [common, studio, wordpress]

- question: How can I request a feature?
  answer: As a part of our <a href="/changelog/" target="_blank">changelog</a> you can let us know what you'd like to see in a future update. You can use our <a href="/roadmap/#request-feature" target="_blank">request feature</a> form for that. We're looking forward to hear from you!
  areas: [common, studio, wordpress]
