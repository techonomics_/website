---
layout: examples-article
base: ../../
permalink: /examples/order-form/
title: Order Form - Tripetto Examples
description: Use Tripetto to create a smart order form to calculate prices for the selected products and services. And give some discount on the go.
article_title: Order Form
article_description: Use Tripetto to create a smart order form to calculate prices for the selected products and services. And give some discount on the go. This example uses the <strong>classic form face</strong>.
example_id: order-form
example_purpose: calculator
example_face: classic
example_face_name: Classic
example_questions: [text-single, email-address, paragraph, picture-choice, number]
example_logic: [branch, piping, send-email, calculator, iteration]
url_runner: https://tripetto.app/run/6RG6DK4TE9
url_template: https://tripetto.app/template/3HOJGMQROB
---
