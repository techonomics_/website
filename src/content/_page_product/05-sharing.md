---
base: ../
---

<section class="product-sharing content">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 shape-before" id="sharing">
        {% include icon-chapter.html chapter='sharing' size='normal' name='Easy Sharing' %}
        <span class="caption caption-rotated">Send out</span>
        <h2 class="palette-sharing shape-after">Share where and how you wish with <span>flexible publication.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10 col-xl-9">
        <p>Tripetto lets you choose how to deploy forms and surveys for collecting your data. Could be somewhere on a <strong>shareable URL</strong> or inside a website with the easily <strong>pastable embed code</strong>.</p>
      </div>
    </div>
    <div class="row">
      <div class="col product-sharing-visual">
        <a href="{{ page.base }}tutorials/sharing-forms-and-surveys-with-a-shareable-link/" target="_blank" class="product-sharing-option product-sharing-link">
          {% include icons/shareable-link.html %}
        </a>
        <a href="{{ page.base }}tutorials/embedding-forms-and-surveys/" target="_blank" class="product-sharing-option product-sharing-embed">
          {% include icons/embed.html %}
        </a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}tutorials/#sharing" class="hyperlink palette-sharing"><span>How to share forms and surveys</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
