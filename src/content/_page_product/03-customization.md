---
base: ../
---

<section class="product-customization content">
  <div class="container container-content">
    <div class="row">
      <div class="col-lg-10 col-xl-9 shape-before" id="customization">
        {% include icon-chapter.html chapter='customization' size='normal' name='Customization' %}
        <span class="caption caption-rotated">Dress up</span>
        <h2 class="palette-customization">Make your creation look the part with <span>convertible faces.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10">
        <p>Not all forms and surveys need the same appearance. Switch easily between <strong>totally different experiences</strong> as you’re designing to see what looks and feels best for the occasion.</p>
      </div>
    </div>
  </div>
  <div class="container">
    <ul class="row carousel-buttons">
      <li class="active" data-target="#carouselProductCustomization" data-slide-to="0">
        <div>
          {% include icon-face.html face='autoscroll' size='big' name='Autoscroll Form Face' template='background' %}
          <h3>Autoscroll</h3>
          <p class="explanation">The autoscroll face presents <strong>one question at a time</strong> and is akin to Typeform’s conversational format.</p>
          <hr />
        </div>
      </li>
      <li data-target="#carouselProductCustomization" data-slide-to="1">
        <div>
          {% include icon-face.html face='chat' size='big' name='Chat Form Face' template='background' %}
          <h3>Chat</h3>
          <p class="explanation">The chat face presents all questions and answers as <strong>speech bubbles</strong> and is partly inspired by Landbot.</p>
          <hr />
        </div>
      </li>
      <li data-target="#carouselProductCustomization" data-slide-to="2">
        <div>
          {% include icon-face.html face='classic' size='big' name='Classic Form Face' template='background' %}
          <h3>Classic</h3>
          <p class="explanation">The classic face presents question fields in a <strong>traditional format</strong> as often seen in SurveyMonkey and the likes.</p>
          <hr />
        </div>
      </li>
    </ul>
    <div class="row carousel-splitter">
      <div class="col-12">
        <hr />
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselProductCustomization" class="carousel slide carousel-fade">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-12">
                  <img src="{{ page.base }}images/scenes/autoscroll-customer-satisfaction.png" alt="Screenshots of a customer satisfaction survey (with NPS) in the autoscroll form face, shown on a tablet and a mobile phone." />
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <p class="explanation">The autoscroll face supports further <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank"><strong>customization</strong></a> of:</p>
                  <div class="form-properties">
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/fonts.html %}Fonts</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/colors.html %}Colors</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/buttons.html %}Buttons</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/backgrounds.html %}Backgrounds</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/input-controls.html %}Input Controls</a>
                    <a href="{{ page.base }}help/articles/discover-additional-options-for-autoscroll-form-face/" target="_blank" class="form-property palette-customization">{% include icons/scroll-direction.html %}Scroll Direction</a>
                    <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/translations.html %}Labels/Translations</a>
                    <a href="{{ page.base }}pricing/" target="_blank" class="form-property palette-customization">{% include icons/remove-branding.html %}Remove Tripetto branding</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <img src="{{ page.base }}images/scenes/chat-wedding-rsvp.png" alt="Screenshots of a wedding RSVP form in the chat form face, shown on a tablet and a mobile phone." />
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <p class="explanation">The chat face supports further <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank"><strong>customization</strong></a> of:</p>
                  <div class="form-properties">
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/fonts.html %}Fonts</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/colors.html %}Colors</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/buttons.html %}Buttons</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/backgrounds.html %}Backgrounds</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/input-controls.html %}Input Controls</a>
                    <a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/" target="_blank" class="form-property palette-customization">{% include icons/chat-bubbles.html %}Chat Bubbles</a>
                    <a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/" target="_blank" class="form-property palette-customization">{% include icons/chat-avatar.html %}Chat Avatar</a>
                    <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/translations.html %}Labels/Translations</a>
                    <a href="{{ page.base }}pricing/" target="_blank" class="form-property palette-customization">{% include icons/remove-branding.html %}Remove Tripetto branding</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <img src="{{ page.base }}images/scenes/classic-restaurant-reservation.png" alt="Screenshots of a restaurant reservation form in the classic form face, shown on a tablet and a mobile phone." />
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <p class="explanation">The classic face supports further <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank"><strong>customization</strong></a> of:</p>
                  <div class="form-properties">
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/fonts.html %}Fonts</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/colors.html %}Colors</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/buttons.html %}Buttons</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/backgrounds.html %}Backgrounds</a>
                    <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/input-controls.html %}Input Controls</a>
                    <a href="{{ page.base }}help/articles/discover-additional-options-for-classic-form-face/" target="_blank" class="form-property palette-customization">{% include icons/form-appearance.html %}Form Appearance</a>
                    <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank" class="form-property palette-customization">{% include icons/translations.html %}Labels/Translations</a>
                    <a href="{{ page.base }}pricing/" target="_blank" class="form-property palette-customization">{% include icons/remove-branding.html %}Remove Tripetto branding</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}tutorials/#customization" class="hyperlink palette-customization"><span>How to customize form and survey faces</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
<script src="{{ page.base }}js/carousel-product-customization.js?v={{ site.cache_version }}"></script>
