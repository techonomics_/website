---
base: ../
---

<section class="product-intro block-first content">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-xl-10 shape-before shape-after">
        <h1>A pretty different form tool.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-lg-9 intro-text">
        <p>Tripetto lets you create deeply conversational form and survey interactions with all of your audiences to <strong>optimize completion rates</strong> and <strong>gain valuable insights</strong>.</p>
      </div>
    </div>
    <div class="row product-anchors">
      <div class="col-xl-11">
        <ul class="anchors d-flex justify-content-between">
          <li>
            <a href="#build">
              {% include icon-chapter.html chapter='build' size='big' name='Visual Builder' %}
              <span>Visual Builder</span>
            </a>
          </li>
          <li>
            <a href="#logic">
              {% include icon-chapter.html chapter='logic' size='big' name='Advanced Logic' %}
              <span>Advanced Logic</span>
            </a>
          </li>
          <li>
            <a href="#customization">
              {% include icon-chapter.html chapter='customization' size='big' name='Customization' %}
              <span>Customization</span>
            </a>
          </li>
          <li>
            <a href="#automations">
              {% include icon-chapter.html chapter='automations' size='big' name='Automations' %}
              <span>Automations</span>
            </a>
          </li>
          <li>
            <a href="#sharing">
              {% include icon-chapter.html chapter='sharing' size='big' name='Easy Sharing' %}
              <span>Easy Sharing</span>
            </a>
          </li>
          <li>
            <a href="#hosting">
              {% include icon-chapter.html chapter='hosting' size='big' name='Flexible Storage' %}
              <span>Flexible Storage</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
