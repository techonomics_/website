---
base: ../../
---

<section class="pricing-wordpress-free pricing-jumbo">
  <div class="container jumbo-container">
    <div class="row jumbo-row">
      <div class="col jumbo-col palette-wordpress">
        <div class="row jumbo-content">
          <div class="col-md-6 col-lg-7 jumbo-content-left">
            <span>Standard plugin</span>
            <h2>Free Plan</h2>
            <p>The <strong>standard WordPress plugin is free</strong>. What’s the catch? Standard is for a single site and all forms have Tripetto branding. We offer premium upgrades for removing it and for other select advanced features.</p>
          </div>
          <div class="col-md-6 col-lg-5 jumbo-content-right">
            <div class="pricing-price palette-purple">
              <div class="pricing-price-prefix">
                <div class="valuta">$</div>
                <div class="amount">0</div>
                <div class="decimals">.00</div>
              </div>
              <div class="pricing-price-suffix">
                <span class="conditions-title">Per plugin.</span>
                <span class="conditions-subtitle">Forever.</span>
              </div>
            </div>
            <ul class="pricing-features fa-ul">
              <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">All Tripetto features</a></li>
              <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited forms</li>
              <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited logic</li>
              <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited questions</li>
              <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited responses</li>
              <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Single-site installation</li>
              <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Removable Tripetto branding<sup>1</sup></li>
              <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Webhooks<sup>1</sup></li>
              <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Slack notifications<sup>1</sup></li>
              <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Multisite installations<sup>1</sup></li>
            </ul>
            <a href="{{ site.url_wordpress_plugin }}" class="button button-full" target="_blank">Get the free plugin</a>
            <small>1. Available as <a href="#premium" class="anchor">premium upgrades</a>.</small>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col jumbo-footer jumbo-footer-hyperlinks">
      <ul class="hyperlinks">
        <li><a href="#premium" class="hyperlink hyperlink-small anchor"><span>See premium upgrades</span><i class="fas fa-arrow-down"></i></a></li>
        <li><a href="{{ page.base }}wordpress/" class="hyperlink hyperlink-small"><span>About the Tripetto WordPress plugin</span><i class="fas fa-arrow-right"></i></a></li>
      </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>

