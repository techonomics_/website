---
base: ../
---

<section class="pricing-wordpress-hero block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-8 shape-before">
        <h1>WP Plugin</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8 shape-after">
        <p>The <strong>standard WordPress plugin is completely free</strong>. We offer premium upgrades for select features and removing Tripetto branding. There’s even an LTD 🚀!</p>
      </div>
    </div>
  </div>
</section>
