---
layout: help-category
base: ../../../
permalink: /help/wordpress/styling-and-customization/
title: Styling and customization in the WordPress plugin - Tripetto Help Center
description: Learn how to customize your form experience, styling and translations.
category_area: wordpress
category_id: customization
redirect_from:
- /help/wordpress/customize-forms/
---
