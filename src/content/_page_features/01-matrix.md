---
base: ../
---

<section class="features-matrix matrix matrix-compare">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-head">
            <thead>
              <tr>
                <th scope="col" class="matrix-nav shape-before">
                  <div>
                    <span role="button" class="matrix-nav-left"><i class="fas fa-chevron-left"></i></span>
                    <span role="button" class="matrix-nav-right"><i class="fas fa-chevron-right"></i></span>
                  </div>
                  <img src="{{ page.base }}images/features/matrix.svg" alt="Illustration representing the compare matrix." />
                </th>
                <th scope="col">
                  <div class="matrix-area palette-studio">
                    <span>Studio</span>
                    <small>Tripetto as web app</small>
                    <a href="{{ site.url_app }}" target="_blank" class="button button-full">Start building now</a>
                  </div>
                </th>
                <th scope="col">
                  <div class="matrix-area palette-wordpress">
                    <span>Plugin</span>
                    <small>Tripetto inside WordPress</small>
                    <a href="{{ site.url_wordpress_plugin }}" target="_blank" class="button button-full">Get the free plugin</a>
                  </div>
                </th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
    {% assign groups = site.data.features-matrix-groups | where_exp: "item", "item.show contains 'compare'" %}
    {% for group in groups %}
    {% assign features = site.data.features-matrix | where_exp: "item", "item.group == group.id" | where_exp: "item", "item.show contains 'compare'" %}
    {% if features.size > 0 %}
    <div class="row">
      <div class="col-12">
        <div class="matrix-title"><h3>{{ group.title }}</h3></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-body">
            {% if group.navigation %}
            <thead class="d-md-none">
              <tr>
                <th scope="col" class="matrix-nav">
                  <div class="d-md-none">
                    <span role="button" class="matrix-nav-left"><i class="fas fa-chevron-left"></i></span>
                    <span role="button" class="matrix-nav-right"><i class="fas fa-chevron-right"></i></span>
                  </div>
                </th>
                <th scope="col">
                  <div class="products-label">Tripetto Studio</div>
                </th>
                <th scope="col">
                  <div class="products-label">Tripetto WP Plugin</div>
                </th>
              </tr>
            </thead>
            {% endif %}
            <tbody>
            {% for feature in features %}
              <tr>
                <td class="matrix-body-row"><h4>{% if feature.icon %}{% assign icon_url = "icons/" | append: feature.icon %}{% include {{ icon_url }}.html %}{% endif %}{% if feature.image %}<span><img src="{{ page.base | append: feature.image }}" alt="{{ feature.title }}" /></span>{% endif %}<span>{{ feature.title }}</span></h4>{% if feature.subtitle %}<small>{{ feature.subtitle }}</small>{% endif %}</td>
                <td>
                {% if feature.studio == true %}
                  <div class="matrix-yes"><i class="fas fa-check fa-fw"></i><span>Yes</span></div>
                {% elsif feature.studio == false %}
                  <div class="matrix-no"><i class="fas fa-times fa-fw"></i><span>No</span></div>
                {% else %}
                  <div>{{ feature.studio }}</div>
                {% endif %}
                {% if feature.studio_subtitle %}
                  <small>{{ feature.studio_subtitle }}</small>
                {% endif %}
                {% if feature.studio_premium == true %}
                  <small class="premium"><a href="{{ page.base }}pricing/studio/" target="_blank"><i class="fas fa-crown fa-fw"></i>Premium feature</a></small>
                {% endif %}
                </td>
                <td>
                {% if feature.wordpress == true %}
                  <div class="matrix-yes"><i class="fas fa-check fa-fw"></i><span>Yes</span></div>
                {% elsif feature.wordpress == false %}
                  <div class="matrix-no"><i class="fas fa-times fa-fw"></i><span>No</span></div>
                {% else %}
                  <div>{{ feature.wordpress }}</div>
                {% endif %}
                {% if feature.wordpress_subtitle %}
                  <small>{{ feature.wordpress_subtitle }}</small>
                {% endif %}
                {% if feature.wordpress_premium == true %}
                  <small class="premium"><a href="{{ page.base }}pricing/wordpress/" target="_blank"><i class="fas fa-crown fa-fw"></i>Premium feature</a></small>
                {% endif %}
                </td>
              </tr>
            {% endfor %}
              <tr class="matrix-cta">
                <th scope="row"></th>
                <td><a href="{{ site.url_app }}" target="_blank">Start building for free<i class="fas fa-arrow-right"></i></a></td>
                <td><a href="{{ site.url_wordpress_plugin }}" target="_blank">Get WordPress plugin<i class="fas fa-arrow-right"></i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {% endif %}
    {% endfor %}
  </div>
</section>

<script src="{{ page.base }}js/matrix.js?v={{ site.cache_version }}"></script>
