---
base: ../
---

<section class="logic-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-xl-9 shape-before">
        <h1>Boost completion rates by using the <span>right logic.</span></h1>
      </div>
      <div class="col-lg-9 col-xl-8 shape-after">
        <p>Apply logic to your form and survey flows visually on the go to make them smarter and only ask your audience the right questions for optimal response.</p>
        <small><span>🎯 Use right logic</span><i class="fas fa-arrow-right"></i><span>🤯 Create smart forms</span><i class="fas fa-arrow-right"></i><span>🚀 Boost completion rates</span></small>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
