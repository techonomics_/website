---
base: ../
---

<section class="logic-block logic-branch" id="branch_logic">
    <div class="container">
        <div class="row block-intro">
            <div class="col-md-11 col-xl-9">
                <h2>Branch logic</h2>
                <p>Branch logic enables you to determine the appropriate follow-up questions for individual respondents, based on their previously given answers. With the various so-called culling modes, you can create just a simple follow-up question or even whole new tracks, and even loop through a follow-up for each selected answer.</p>
                <small>Others might call it <i>logic</i>, <i>logic flows</i>, <i>question logic</i> and/or <i>routing</i>.</small>
            </div>
        </div>
        <div class="row block-resources">
            <div class="col-lg-7">
                <h3 class="resources-example">Example</h3>
                <p>Let's say you're running a sport shop that wants to gather some customer feedback about their products. In such a case you could start with asking if your respondent has bought anything at all. If not, you could ask why not. And, in case they did made a purchase, the next step is to determine which product(s) were purchased. And after that, for each purchased product you can loop trough a set of follow-up questions to get the right feedback.</p>
                <ul class="fa-ul resources-example">
                    <li><a href="{{ site.url_app }}/collect/H07HLCMNN8" target="_blank"><span class="fa-li"><i class="fas fa-running"></i></span>Run this example form</a></li>
                    <li><a href="{{ site.url_app }}/template/8009NFU8P1" target="_blank"><span class="fa-li"><i class="fas fa-pencil-ruler"></i></span>View this template in the builder</a></li>
                    <li><a href="{{ site.url_app }}" target="_blank"><span class="fa-li"><i class="fas fa-arrow-right"></i></span>Start from scratch</a></li>
                </ul>
            </div>
            <div class="col-lg-5 offset-xl-1 col-xl-4">
                <h3>Help Center</h3>
                <ul class="fa-ul resources-help-center">
                    <li><a href="{{ page.base }}help/articles/how-to-create-an-other-option/" target="_blank"><span class="fa-li"><i class="fas fa-life-ring"></i></span>How to create an 'Other...' option</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank"><span class="fa-li"><i class="fas fa-life-ring"></i></span>How to set a follow-up based on respondents' answers</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-multiple-selected-options/" target="_blank"><span class="fa-li"><i class="fas fa-life-ring"></i></span>How to repeat follow-up for multiple selected options</a></li>
                    <li><a href="{{ page.base }}help/" target="_blank"><span class="fa-li"><i class="fas fa-arrow-right"></i></span>Visit our Help Center</a></li>
                </ul>
                <h3>Video Tutorials</h3>
                <ul class="fa-ul resources-tutorials">
                    <li><a data-toggle="modal" data-target="#videoModal" data-video="pb01iB9UhDQ"><span class="fa-li"><i class="fab fa-youtube"></i></span>Watch tutorial about simple logic</a></li>
                    <li><a data-toggle="modal" data-target="#videoModal" data-video="RfD9bXK9Sos"><span class="fa-li"><i class="fab fa-youtube"></i></span>Watch tutorial about branch logic</a></li>
                    <li><a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank"><span class="fa-li"><i class="fas fa-arrow-right"></i></span>Visit our YouTube channel</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
