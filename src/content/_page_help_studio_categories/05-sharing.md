---
layout: help-category
base: ../../../
permalink: /help/studio/sharing-forms-and-surveys/
title: Sharing forms and surveys in the studio - Tripetto Help Center
description: See how you can distribute your forms from the studio to your audience.
category_area: studio
category_id: sharing
redirect_from:
- /help/studio/run-forms/
---
