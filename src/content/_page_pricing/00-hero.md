---
base: ../
---

<section class="pricing-hero block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-8 shape-before">
        <h1>Pricing</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8 shape-after">
        <p>Quite a lot about Tripetto is free. However, premium plans are required for select features and removing Tripetto branding. <strong>The idea is to keep things fair.</strong></p>
      </div>
    </div>
  </div>
</section>
