---
base: ../
---
{% assign reviews-left = site.data.reviews | where_exp: "item", "item.reviews_position == 'left'" %}
{% assign reviews-right = site.data.reviews | where_exp: "item", "item.reviews_position == 'right'" %}
<section class="reviews-list">
  <div class="container">
    <div class="row">
      <div class="col-md-6 shape-before">
        <ul class="reviews">
          {% for item in reviews-left %}
          <li>
            <div>
              <div class="review-rating">
                {% for i in (1..item.rating) %}
                <i class="fas fa-star"></i>
                {% endfor %}
              </div>
              <a href="{{ item.url }}" target="_blank"><h4>{{ item.title }}</h4></a>
              <p>{{ item.text }}</p>
              <div class="review-footer">
                <span>{{ item.author }}</span>
                <small>{{ item.date }}</small>
              </div>
            </div>
          </li>
          {% endfor %}
        </ul>
      </div>
      <div class="col-md-6 reviews-right">
        <ul class="reviews">
          {% for item in reviews-right %}
          <li>
            <div>
              <div class="review-rating">
                {% for i in (1..item.rating) %}
                <i class="fas fa-star"></i>
                {% endfor %}
              </div>
              <a href="{{ item.url }}" target="_blank"><h4>{{ item.title }}</h4></a>
              <p>{{ item.text }}</p>
              <div class="review-footer">
                <span>{{ item.author }}</span>
                <small>{{ item.date }}</small>
              </div>
            </div>
          </li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>
<script src="{{ page.base }}js/reviews.js?v={{ site.cache_version }}"></script>
