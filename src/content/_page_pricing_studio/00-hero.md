---
base: ../
---

<section class="pricing-studio-hero block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-8 shape-before">
        <h1>Studio</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10 col-xl-9 shape-after">
        <p><strong>The studio is free.</strong> We offer an optional upgrade per form for removing Tripetto branding, because we need visibility or sponsorships to have a chance of shaking the big guys 🙏🏼.</p>
      </div>
    </div>
  </div>
</section>
