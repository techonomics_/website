---
base: ../
---

<section class="calculator-features-matrix matrix">
  <div class="container">
    {% assign group_count = 0 %}
    {% assign groups = site.data.calculator-matrix-groups %}
    {% for group in groups %}
    {% assign features = site.data.calculator-matrix | where_exp: "item", "item.group == group.id" %}
    {% if features.size > 0 %}
    <div class="row">
      <div class="col-12">
        <div class="matrix-title{% if group_count == 0 %} matrix-hint{% endif %}"><h3>{{ group.title }}</h3></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-body">
            <tbody>
              {% for feature in features %}
              <tr>
                <td class="matrix-body-row{% if feature.indent %} matrix-body-row-indent{% endif %}"><h4>{{ feature.title }}</h4></td>
                <td>
                  <div>{{ feature.description | markdownify }}</div>
                </td>
              </tr>
              {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {% assign group_count = group_count | plus: 1 %}
    {% endif %}
    {% endfor %}
    <div class="row">
      <div class="col matrix-hyperlink">
        <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="hyperlink palette-logic"><span>See how to make calculations</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
