---
base: ../
---

<section class="calculator-features-operators">
  <div class="container">
    <div class="row">
      <div class="col">
        <ul><li><img src="{{ page.base }}images/calculator/add.svg" alt="Icon representing an addition" /><h2>Add</h2></li><li><img src="{{ page.base }}images/calculator/subtract.svg" alt="Icon representing a subtraction" /><h2>Subtract</h2></li><li><img src="{{ page.base }}images/calculator/multiply.svg" alt="Icon representing a multiplication" /><h2>Multiply</h2></li><li><img src="{{ page.base }}images/calculator/divide.svg" alt="Icon representing a division" /><h2>Divide</h2></li><li><img src="{{ page.base }}images/calculator/equal.svg" alt="Icon representing an equation" /><h2>Equal</h2></li></ul>
      </div>
    </div>
  </div>
</section>
