---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/performing-actions-inside-your-forms-and-surveys/
title: Performing actions inside your forms and surveys - Tripetto Tutorials
description: A tutorial about action blocks to let your forms and surveys perform certain actions.
article_title: Performing actions inside your forms and surveys
author: jurgen
time: 2
category_id: logic
common_content_core: true
---
<p>With action blocks you can let your form perform certain actions, like performing a calculation, using a variable, or send an automated email.</p>

<div class="article-content-core">
<h2 id="concept">Concept</h2>
<p>Action blocks can be added like 'normal' question blocks. You can add them anywhere in your form and for an unlimited amount.</p>
<p>The difference is that an action block does not ask for a certain input from the respondent, but it performs a certain <strong>action</strong> in the background. This makes your forms and surveys even more powerful.</p>

<h2 id="action_blocks">Action blocks</h2>
<p>The following action blocks are available:</p>

<h3 id="calculator">Calculator</h3>
<p>The calculator is a very powerful action block that lets you perform calculations with given answers inside the form. Calculator blocks are fully flexible. You can use the outcomes anywhere in the form. Simply by showing the outcome to your respondent, but also to perform follow-up calculations.</p>

<h3 id="hidden_field">Hidden field</h3>
<p>Hidden fields let the form gather certain data that you can use inside the form, for example a variable from the querystring in the URL.</p>

<h3 id="raise_error">Raise error</h3>
<p>The raise error block prevents the form from being completed. Why would you want that? Well, imagine a respondent does not meet your requirements to fill out a certain form, based on the given answers. To keep your results clean, you can prevent them from completing the form.</p>

<h3 id="send_email">Send email</h3>
<p>This sends an automated email to a fixed email address, or even to an email address that your respondent entered in the form. And you can use all entered form data directly in the email.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about action blocks inside your forms:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'action-blocks-calculator' or item.article_id == 'action-blocks'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
