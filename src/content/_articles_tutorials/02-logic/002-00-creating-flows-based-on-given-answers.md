---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/creating-flows-based-on-given-answers/
title: Creating flows based on given answers - Tripetto Tutorials
description: A tutorial about branches to create flows based on answers given by your respondents.
article_title: Creating flows based on given answers
author: jurgen
time: 2
category_id: logic
common_content_core: true
---
<p>Make your forms and surveys smart by reacting to the given answers of your respondents and only show the right questions.</p>

<div class="article-content-core">
<h2 id="branch_logic">Branch logic</h2>
<p>You can use <strong>branch logic</strong> to check if a respondent meets your conditions to get a certain follow-up in your form (or not). This way you make your forms as compact as possible, resulting in higher completion rates.</p>
<p>Inside the form builder you use <strong>branch conditions</strong> and <strong>branch behavior</strong> to compose the right branch track.</p>
<h3 id="branch_conditions">Branch conditions</h3>
<p>The branch conditions determine which check(s) should be performed to show a certain follow-up (or not). You can combine endless conditions to get the perfect fit.</p>
<h3 id="branch_behavior">Branch behaviors</h3>
<p>The branch behavior determines how the branch conditions should be judged:</p>
<ul>
  <li>Check for <i>one</i> condition match;</li>
  <li>Check for <i>all</i> conditions match;</li>
  <li>Check for <i>each</i> condition match.</li>
</ul>

<h2 id="skip_logic">Skip logic</h2>
<p>After each branch you can determine what should happen next with <strong>skip logic</strong>. For example skip to a certain part of your form, or skip to the end with a closing message.</p>
<p>The form builder helps you with that by setting the right <strong>branch ending</strong>.</p>
<h3 id="branch_endings">Branch endings</h3>
<p>The branch ending determines how the form should continue after that branch:</p>
<ul>
  <li>Continue with the next cluster or branch;</li>
  <li>Jump to a specific point;</li>
  <li>Jump to end;</li>
  <li>End with closing message.</li>
</ul>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about branch and jump logic:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'logic-basics' or item.article_id == 'logic-flow'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
