---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/self-hosting-form-and-survey-data/
title: Self-hosting form and survey data - Tripetto Tutorials
description: Learn how to take control of form and survey data with self-hosting.
article_title: Self-hosting form and survey data
author: mark
time: 2
time_video: 1
category_id: hosting
common_content_core: true
---
<p>In Tripetto you can also take control over your data storage by self-hosting your forms and/or entry data.</p>

<div class="article-content-core">
<h2 id="self-hosting">Privacy and security</h2>
<p>Privacy is an important subject nowadays. Whereas before your respondents had no idea where and how their data was stored, it gets more important nowadays to have your data under control, so you know for sure the privacy and security of your data is settled. The evolution of privacy legislation (like GDPR in Europe) contributes to that.</p>
<p>At Tripetto we thinks it's important to embrace this, so we have taken account of this from the cores of our platform. It depends on how you use Tripetto, but in all cases there are advanced possibilities to take control over the data storage by <strong>self-hosting</strong> it.</p>

<h2 id="studio">Self-hosting in studio</h2>
<p>In the Tripetto Studio at tripetto.app you can choose to self-host your data when you embed the form. The studio gives you some settings to determine how to host the form structure (at Tripetto vs inside the embed code) and how to host the entry data (at Tripetto vs self-hosted).</p>
<p>By using the built-in Tripetto data API and setting up your own endpoint to save the data, you can fully take control over the data storage.</p>

<h3 id="help">Help articles</h3>
<p>In our Help Center you can find more detailed help articles about data control in the studio:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'data-studio'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top='studio' category=data_category.name palette-category=articles.category_id type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>

<div class="article-content-core">
<hr/>
<h2 id="wordpress">Self-hosting in WordPress plugin</h2>
<p>In the WordPress plugin it gets even easier to take control over your data, as all data (both form structures and entry data) is always saved inside your own WordPress databases by design. So actually you don't even have a choice in WordPress: you always have full control over your data. No connection at all with Tripetto or any other third-parties.</p>
<h3 id="info-wordpress">More information</h3>
<p>On our WordPress page we tell you more about the data storage in the WordPress plugin:</p>
</div>
<ul class="tiles tiles-two">
    {% assign wordpress_url = page.base | append: "wordpress/" %}
    {% include tile.html url=wordpress_url target=true title='More information about the WordPress plugin' palette-top='wordpress' type='Information' time=3 %}
</ul>

<div class="article-content-core">
<hr/>
<h2 id="developers">Take full control for developers</h2>
<p>Tripetto even adds another level of taking control over your forms and surveys. Our <strong>developers kit</strong> let's you take control over every aspect of your form, with our technology at the core of everything.</p>
<p>The developer kit let's you integrate all our components, like the form builder and our form runners, into your own projects and then let you take full control over how to deal with all form and entry data. And you can even extend all those components with your own requirements by building your own blocks. The possibilities are literally endless.</p>
<h3 id="info-developers">More information</h3>
<p>On our Developers page we tell you more about the possibilities of the developers kit:</p>
</div>
<ul class="tiles tiles-two">
    {% assign developers_url = page.base | append: "developers/" %}
    {% include tile.html url=developers_url target=true title='More information about the developers kit' palette-top='sdk' type='Information' time=5 %}
</ul>
