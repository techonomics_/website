---
base: ../
---
<section class="contact-team">
  <div class="container">
    <div class="row contact-team-members">
        <div class="col-6 col-lg-3">
            <img src="{{ page.base }}images/team/mark-van-den-brink.svg" alt="Mark van den Brink (Founder | Tech)" class="img-fluid" />
            <h3>Mark van den Brink</h3>
            <p>Founder | Tech</p>
            <div>
                <a href="mailto:mark@tripetto.com" class="social mail"><i class="far fa-envelope" title="Mail Mark"></i></a>
                <a href="https://twitter.com/mvandenbrink81" class="social twitter" target="_blank"><i class="fab fa-twitter" title="View Mark at Twitter"></i></a>
                <a href="https://www.linkedin.com/in/mvandenbrink/" class="social linkedin" target="_blank"><i class="fab fa-linkedin-in" title="View Mark at LinkedIn"></i></a>
                <a href="https://gitlab.com/markvandenbrink" class="social gitlab" target="_blank"><i class="fab fa-gitlab" title="View Mark at Gitlab"></i></a>
                <a href="https://www.npmjs.com/~markvandenbrink" class="social npm" target="_blank"><i class="fab fa-npm" title="View Mark at npm"></i></a>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <img src="{{ page.base }}images/team/martijn-wijtmans.svg" alt="Martijn Wijtmans (Founder | Product)" class="img-fluid" />
            <h3>Martijn Wijtmans</h3>
            <p>Founder | Product</p>
            <div>
                <a href="mailto:martijn@tripetto.com" class="social mail"><i class="far fa-envelope" title="Mail Martijn"></i></a>
                <a href="https://twitter.com/martijnwijtmans" class="social twitter" target="_blank"><i class="fab fa-twitter" title="View Martijn at Twitter"></i></a>
                <a href="https://www.linkedin.com/in/martijn-wijtmans-2276b011b/" class="social linkedin" target="_blank"><i class="fab fa-linkedin-in" title="View Martijn at LinkedIn"></i></a>
                <a href="https://gitlab.com/martijnwijtmans" class="social gitlab" target="_blank"><i class="fab fa-gitlab" title="View Martijn at Gitlab"></i></a>
                <a href="https://www.npmjs.com/~martijnwijtmans" class="social npm" target="_blank"><i class="fab fa-npm" title="View Martijn at npm"></i></a>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <img src="{{ page.base }}images/team/jurgen-van-den-brink.svg" alt="Jurgen van den Brink (Front-end | Support)" class="img-fluid" />
            <h3>Jurgen van den Brink</h3>
            <p>Front-end | Support</p>
            <div>
                <a href="mailto:jurgen@tripetto.com" class="social mail"><i class="far fa-envelope" title="Mail Jurgen"></i></a>
                <a href="https://twitter.com/jurvandenbrink" class="social twitter" target="_blank"><i class="fab fa-twitter" title="View Jurgen at Twitter"></i></a>
                <a href="https://www.linkedin.com/in/jurgenvandenbrink/" class="social linkedin" target="_blank"><i class="fab fa-linkedin-in" title="View Jurgen at LinkedIn"></i></a>
                <a href="https://gitlab.com/jurgenvandenbrink" class="social gitlab" target="_blank"><i class="fab fa-gitlab" title="View Jurgen at Gitlab"></i></a>
                <a href="https://www.npmjs.com/~jurgenvandenbrink" class="social npm" target="_blank"><i class="fab fa-npm" title="View Jurgen at npm"></i></a>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <img src="{{ page.base }}images/team/martin-van-de-hoef.svg" alt="Martin van de Hoef (Back-end | Devops)" class="img-fluid" />
            <h3>Martin van de Hoef</h3>
            <p>Back-end | Devops</p>
            <div>
                <a href="mailto:martin@tripetto.com" class="social mail"><i class="far fa-envelope" title="Mail Martin"></i></a>
                <a href="https://twitter.com/martinvandehoef" class="social twitter" target="_blank"><i class="fab fa-twitter" title="View Martin at Twitter"></i></a>
                <a href="https://www.linkedin.com/in/martinvandehoef/" class="social linkedin" target="_blank"><i class="fab fa-linkedin-in" title="View Martin at LinkedIn"></i></a>
                <a href="https://gitlab.com/martinvandehoef" class="social gitlab" target="_blank"><i class="fab fa-gitlab" title="View Martin at Gitlab"></i></a>
                <a href="https://www.npmjs.com/~martinvandehoef" class="social npm" target="_blank"><i class="fab fa-npm" title="View Martin at npm"></i></a>
            </div>
        </div>
    </div>
    <div class="row contact-team-contributors">
      <div class="col">Special thanks to <a href="https://www.pablostanley.com/" target="_blank">Pablo Stanley</a> and <a href="https://blush.design/" target="_blank">Blush</a> for the illustrations.</div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
