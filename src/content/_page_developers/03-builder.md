---
base: ../
---

<section class="developers-builder content">
  <div class="container container-content">
    <div class="row">
      <div class="col-lg-10 col-xl-9" id="builder">
        <span class="caption">Integrating the Tripetto builder</span>
        <h2 class="palette-purple">Build Tripetto forms inside your project <span>autonomously.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-10">
        <p>The visual builder is for creating advanced forms with logic and conditional flows on an assisitive storyboard. It can easily be <strong>tightly integrated into any website or app</strong> and works smoothly in mainstream browsers; mouse, touch or pen. Form structures are stored in a JSON-formatted form definition.</p>
      </div>
    </div>
  </div>
  <div class="container container-builder">
    <div class="row">
      <div class="col-md-5 col-lg-4 builder">
        <h3>Using the builder</h3>
        <p class="explanation">The builder is made to run <strong>neatly inside your apps and websites</strong>. Rapidly unlock all of its comprehensive form building capabilities.</p>
        <ul class="hyperlinks-list">
          <li><a href="{{ site.url_docs }}/guide/builder/" target="_blank" class="hyperlink hyperlink-small palette-purple"><span>Builder documentation</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://www.npmjs.com/package/tripetto" target="_blank" class="hyperlink hyperlink-small palette-purple"><span>Integrate the builder</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://gitlab.com/tripetto/builder" target="_blank" class="hyperlink hyperlink-small palette-purple"><span>Builder repo on GitLab</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
      <div class="col-md-7 builder">
        <h3>Builder pricing</h3>
        <p class="explanation">You may embed and integrate the builder in websites and applications for free for personal, academic and evaluation purposes. <strong>A paid license is required for select builder usages in business applications.</strong> Licenses start at $250/month.</p>
        <a href="{{ site.url_sdk_quote }}" target="_blank" class="button button-small">Request SDK quote</a>
      </div>
    </div>
  </div>
  <div class="container container-visual">
    <div class="row row-visual">
      <div class="col-12"><img src="{{ page.base }}images/developers/builder.png" alt="Screenshot of the form builder inside your own project." /></div>
    </div>
    <div class="row">
      <div class="col-12 builder-footer">
        <small>A paid license is required for select builder implementations in business applications. <a href="{{ site.url_sdk_quote }}" target="_blank">Request a SDK quote.</a></small>
      </div>
    </div>
  </div>
</section>
