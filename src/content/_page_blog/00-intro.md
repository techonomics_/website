---
base: ../
---

<section class="blog-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-8 shape-before">
        <h1>Blog</h1>
        <p>Tripetto blogs include articles on product launches, features, showcases, background stories, coding tutorials and <strong>open-hearted stories of the road so far</strong>.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-7">
        <form action="https://tripetto.us16.list-manage.com/subscribe/post?u=6eddefde72cc0b876c8b7b1f9&amp;id=f79a61bb9a" method="post" target="_blank" class="form-inline">
          <label class="sr-only" for="EMAIL_BLOG">Email address to subscribe</label>
          <input type="email" name="EMAIL" class="form-input form-input-placeholder" id="EMAIL_BLOG" placeholder="Type your email for updates">
          <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6eddefde72cc0b876c8b7b1f9_f79a61bb9a" tabindex="-1" value="" /></div>
          <button type="submit" class="button button-wide">Subscribe</button>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
