---
base: ../
---

<section class="help-hero block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-8 shape-before">
        <h1>Help Center</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8 shape-after">
        <p>Things not working right? The help center offers <strong>how-to’s</strong>, <strong>video tutorials</strong> and <strong>FAQ’s</strong>. Find answers directly or navigate to your Tripetto version below.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-xl-8">
        <form action="{{ page.base }}help/search/" method="get" class="help-search form-inline">
          <label class="sr-only" for="search-input">Search all help articles</label>
          <input type="text" name="q" class="form-input form-input-big" id="search-input" placeholder="Search all help articles" />
          <button class="button button-big"><i class="fas fa-search"></i><span>Search</span></button>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
