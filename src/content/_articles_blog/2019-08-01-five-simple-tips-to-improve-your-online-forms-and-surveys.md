---
layout: blog-article
base: ../../
permalink: /blog/five-simple-tips-to-improve-your-online-forms-and-surveys/
title: 5 Simple tips to improve your online forms and surveys - Tripetto Blog
description: Find out how you can improve your online forms and surveys to boost your conversion or response.
article_title: 5 Simple tips to improve your online forms and surveys
article_folder: 20190801
author: jurgen
time: 4
category: background-story
category_name: Background story
tags: [background-story, collector]
---
<p>Let’s face it: life would be much easier if you didn’t have to fill out all these online forms and surveys these days. From a simple newsletter signup, to a purchase form, or even a 30 minutes survey to help out your friend in college. It’s all 😖 <i>blegh</i>…</p>
<p>Unfortunately, until the time computers can read our minds (let’s hope that takes a while 🤐) and fill out the forms for us, we have to deal with them. So why not make them as easy and fun to use as possible?</p>
<p>So, for all researchers, students, form developers and other online form/survey creators:</p>
<p><strong>5 simple tips to improve your online forms and surveys to boost your conversion or response with forms. Let's go!</strong></p>

<h2>1 - Make it friendly, be a person</h2>
<div class="tenor-gif-embed" data-postid="5576725" data-share-method="host" data-width="100%" data-aspect-ratio="2.0"><a href="https://tenor.com/view/forrest-gump-forrest-gump-wave-hello-gif-5576725">Forrest Gump GIF</a> from <a href="https://tenor.com/search/forrest-gifs">Forrest GIFs</a></div>
<p>Think about the last online form you filled out for a minute. I’m betting it was just a list of questions with their corresponding input fields. Not a very appealing experience for anyone…</p>
<p>Making your forms more friendly and personal likely triggers more respondents to fill them out. For example, your first question could be <i>‘First name’</i>, but what if we make that more friendly like <i>‘What’s your first name?’</i>. It gives respondents the feeling they’re talking to a person instead of a boring form.</p>
<p>And, next step, if you use that given first name later on in the survey, your respondent will feel more comfortable during the whole experience. So instead of showing <i>‘Select your hobbies’</i> you could ask <i>‘So Tom, what do you like to do in your spare time?’</i>.</p>
<p>👉 <strong>Quite a difference in tone, right?</strong></p>
<hr />
<h2>2 - Stay on point and skip the rest</h2>
<div class="tenor-gif-embed" data-postid="13052023" data-share-method="host" data-width="100%" data-aspect-ratio="2.0"><a href="https://tenor.com/view/skip-skip-to-the-end-netflix-and-chill-gif-13052023">Skip Skip To The End GIF</a> from <a href="https://tenor.com/search/skip-gifs">Skip GIFs</a></div>
<p>Another typical problem with common forms is the overload of questions. A form or survey often just shows all the questions immediately and that’s it. This way respondents end up filling out all kinds of questions that aren’t meant for them.</p>
<p>An effective way to avoid such needless hassle - and chasing off people - is by making your forms smart and by doing so making them feel like a real conversation. Smart here means that you use previously given answers to determine which follow-up question(s) should be asked.</p>
<p>In the ‘easy’ variant you could think of something like a gender question, followed by questions that are only relevant for the selected gender. You wouldn’t be showing questions for males when your respondent is female.</p>
<p>But to take it even further, let’s get back to the hobbies question. Suppose that’s a question where multiple answers are allowed and for each given answer you want a specific follow-up question, which may vary from hobby to hobby. Instead of asking all follow-up questions about all possible hobbies, only use the selected hobbies and iterate through these with their respective specific follow-up questions. Skip what’s irrelevant. Your form gets shorter for respondents and stays on point.</p>
<p>👉 <strong>It gives your respondent the feeling he’s been heard. Don’t we all like that?</strong></p>
<hr />
<h2>3 - Ask one by one, like in a conversation</h2>
<div class="tenor-gif-embed" data-postid="9191931" data-share-method="host" data-width="100%" data-aspect-ratio="2.0"><a href="https://tenor.com/view/stay-focused-dont-lose-focus-pointing-focus-eye-on-the-prize-gif-9191931">Don't Lose Focus GIF</a> from <a href="https://tenor.com/search/stayfocused-gifs">Stayfocused GIFs</a></div>
<p>We mentioned it before: most forms just overwhelm you with all the questions you need to answer. The result is that respondents lose their focus, because there’s so much going on. It can even discourage them, because it just seems so much work.</p>
<p>In real life you also wouldn’t overwhelming people with 10 questions first and then let them respond to all of your 10 questions. You ask them questions one by one and preferably respond to the answers they give.</p>
<p>The same applies to your online forms and surveys. By asking one question at a time, you let respondents just focus on that particular question. It will better open their minds and therefore improve their answers.</p>
<p>👉 <strong>In short: focus, focus, focus!</strong></p>
<hr />
<h2>4 - Make changes easy</h2>
<div class="tenor-gif-embed" data-postid="5167959" data-share-method="host" data-width="100%" data-aspect-ratio="2.0"><a href="https://tenor.com/view/change-personalities-people-change-people-do-change-selena-gomez-gif-5167959">People Do Change GIF</a> from <a href="https://tenor.com/search/change-gifs">Change GIFs</a></div>
<p>A possible consequence of the conversational approach with one-by-one questions is that respondents get lost in the form. After all, the previously given answers roll out of the screen as they proceed to the next question.</p>
<p>Therefore, it is important respondents can go back to earlier parts of the form and change their initial input. In real life you also can get back on a given answer, right?</p>
<p>When someone changes a previous answer, that change can of course affect the way your conversation goes. For example, if someone changes the <i>‘What’s your gender’</i> question from male to female (everything is possible these days 🎉), that can change the whole form flow for this respondent. Make sure you’re preserving and showing everything respondents have answered, so they can easily find what to change. And upon any change take them through the alternative flow.</p>
<p>👉 <strong>Flexibility is key.</strong></p>
<hr />
<h2>5 - Style forms to fit your audience</h2>
<div class="tenor-gif-embed" data-postid="5666824" data-share-method="host" data-width="100%" data-aspect-ratio="2.0"><a href="https://tenor.com/view/queen-bey-beyonce-style-stylish-fashion-gif-5666824">Queen Bey Beyonce GIF</a> from <a href="https://tenor.com/search/queenbey-gifs">Queenbey GIFs</a></div>
<p>Coming back to our first point to making your forms more friendly: friendliness can be achieved by the tone of your questions, but the appearance of your form can also have a big effect on the way people experience your form or survey. Forms and surveys are often poorly styled, which can have a negative effect on the willingness to fill them out.</p>
<p>Paying a bit more attention to the styling of it all can make a big difference. People rather fill out an appealing form than a basic and messy one.</p>
<p>Also keep in mind your audience. Not every styling is suitable for every audience. For example, a serious survey asks for a clean styling, but a sports quiz can have a more outspoken and less serious style.</p>
<p>👉 <strong>Release the artist in you, but do it gently.</strong></p>
<hr />
<h2>How to use this all?</h2>
<p>Basically, it’s quite easy: by making your forms feel like a real conversation your audience will be more likely to fill out your forms.</p>
<p>Unfortunately, not all form/survey solutions give you the right tools to achieve this. The most important shortcoming is the lack of simple to use logic in your form builder. It’s often non-existing, or paywalled, even though proper logic is so essential to smart, truly conversational forms.</p>
<h3>And that’s where we come in</h3>
<p>We’re Tripetto from Amsterdam and we provide multiple solutions to easily build and share smart flowing forms. We rather call them conversational experiences, because respondents don’t have the feeling they’re filling out a form. They’re just having a nice talk to a form that listens.</p>
<p>While the output is of course the most important, we’ve also managed to make it easy (even fun) to create your smart flowing forms on the self-organizing storyboard. Not an endless list of all your questions without any sense of flow inside your form, but a visualization of your complete form.</p>
<h3>Let's get you started 🚀</h3>
<p>Tripetto comes in three flavors. Just start with the <a href="{{ page.base }}studio/">studio</a> to see all of the powers and from there on determine if you also could use the <a href="{{ page.base }}wordpress/">WordPress plugin</a> or the <a href="{{ page.base }}developers/">SDK</a>.</p>
<p>To get you going we made some <a href="{{ page.base }}examples/">examples</a> and <a href="{{ page.base }}tutorials/">tutorials</a>. For any other questions/requests/missing features, just <a href="{{ page.base }}contact/">drop us a line</a>.</p>
<script type="text/javascript" async src="https://tenor.com/embed.js"></script>
