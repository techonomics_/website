---
layout: help-article
base: ../../../
permalink: /help/articles/learn-the-basics-of-the-form-builder/
title: Learn the basics of the form builder - Tripetto Help Center
description: Learn the basics and gestures to control the form builder.
article_title: Learn the basics of the form builder
article_id: builder
author: martijn
time: 2
time_video: 3
category_id: build
subcategory: build_basics
areas: [studio, wordpress]
---
<p>The Tripetto form builder is a bit different from existing tools, but you'll know why and love it almost instantly when you spend a minute getting the hang of it. So let us show you how to use it.</p>

<h2 id="concept" data-anchor="Concept">The concept</h2>
<p>Our form builder is not just a list of questions. As we want to make it easy to create smart forms with logic, we use a <strong>storyboard</strong>. This board gives a visual presentation of the flows inside your form, making it much better understandable what's happening in your form structure.</p>

<h2 id="adding-and-editing">Adding and editing</h2>
<p>Let's start with an empty storyboard. And let's say we just want to add some questions. You can add questions by clicking the <code><i class="fas fa-plus"></i></code> icon. A new question block will be created and you can fill in this question block by clicking it once.</p>
<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/79ZbM26uXxU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Short video on how to add and edit question blocks</figcaption>
</figure>
<p>For a more in-depth tutorial on adding and editing question blocks, please have a look at <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">this article</a>.</p>

<h2 id="gestures">Gestures</h2>
<p>There are a few controls you need to know to master the form builder in Tripetto.</p>

<h3 id="arranging">Arranging</h3>
<p>If you have multiple question blocks on your storyboard (and maybe some logic in it as well), you can arrange your question blocks, for example to change the order of two questions. You can do that by dragging and dropping a block.</p>
<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/1KvKSf-od4o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Short video on how to arrange blocks</figcaption>
</figure>

<h3 id="panning">Panning</h3>
<p>You can navigate through your form (especially with large forms) by holding the storyboard and drag it into the direction you want.</p>
<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/7nFshp1iPBM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Short video on how to move around the storyboard</figcaption>
</figure>

<h3 id="zooming">Zooming</h3>
<p>If you have a large form with lots of branches, you can zoom out to see the whole structure of your form, or zoom in to see a particular part of your form.</p>
<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/Cdkn-JJCTDE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Short video on how to use zooming</figcaption>
</figure>
<hr />

<h2>More in-depth tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" alt="YouTube logo" />
    </div>
  </a>
</div>
