---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-a-welcome-message/
title: How to add a welcome message - Tripetto Help Center
description: Learn more about the welcome message that you can add before your form starts.
article_title: How to add a welcome message
article_folder: editor-start
author: jurgen
time: 2
category_id: build
subcategory: build_messages
areas: [studio, wordpress]
---
<p>Learn more about the welcome message that you can add before your form starts.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the welcome message to gently welcome your respondents and introduce them to your form. This gives your respondents the feeling they know what to expect from the form and thus making it more comfortable to start filling out the form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo.gif" alt="Screenshot of a welcome message in Tripetto" />
  <figcaption>Demonstration of a welcome message.<br/><a href="https://unsplash.com/photos/6wAGwpsXHE0" target="_blank">Photo credits Unsplash</a>.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>To enable the welcome message, open your form inside the form builder. Each form design starts with a green bubble with a <code><i class="fas fa-sign-in-alt"></i></code> icon at the top of the form. By clicking that bubble the pane to add/edit the welcome message opens up.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-add.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The welcome message can be set from the green bubble.</figcaption>
</figure>

<h3 id="features">Features</h3>
<p>The welcome message consists of some features that you can enable, just the way you like/need it. You can see those features on the left of the welcome message pane. The following features are available:</p>
<ul>
  <li><strong>Text</strong> - Enable the <code>Text</code> feature to supply a title to the welcome message.</li>
  <li><strong>Description</strong> - Enable the <code>Description</code> feature to supply a description to the welcome message.</li>
  <li><strong>Image</strong> - Enable the <code>Image</code> feature to add an image to the welcome message. The image must be hosted somewhere else so you can supply the URL to the image;</li>
  <li><strong>Video</strong> - Enable the <code>Video</code> feature to add a video to the welcome message. We support YouTube and Vimeo videos. All you have to do is copy-paste the URL of the video from YouTube/Vimeo and the video will be embedded;</li>
  <li><strong>Button</strong> - Enable the <code>Button</code> feature to add a button to the welcome message to start the form. You can supply the label of the button.</li>
</ul>

<h3 id="form-faces">Form faces</h3>
<p>Tripetto forms can be presented in different ways, that we call <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/" target="_blank">form faces</a>. It depends on the selected form face how the welcome message gets presented and behaves.</p>
