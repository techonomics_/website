---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-checkboxes-block/
title: How to use the checkboxes block - Tripetto Help Center
description: Learn everything you need to know to use the checkboxes block in your forms.
article_title: How to use the checkboxes block
article_folder: editor-block-checkboxes
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-checkboxes-block/
---
<p>Learn everything you need to know to use the checkboxes block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the checkboxes block to let your respondents select multiple items from a list of checkbox choices you give them.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/checkboxes.gif" alt="Screenshot of checkboxes in Tripetto" />
  <figcaption>Demonstration of a checkboxes block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Checkboxes</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="choices">Choices</h3>
<p>For the checkboxes block you can enter the list of choices you want to show to your respondents. You can just add the desired amount of choices and enter the names.</p>
<p>From your list of choices you can open each choice to get to the features of that single choice. Over there you can use these advanced options:</p>
<ul>
  <li>
    <h4>Name</h4>
    <p>This is the name of the choice.</p>
  </li>
  <li>
    <h4>Description</h4>
    <p>By enabling the <code>Description</code> feature, you can extend the choice name with a description, for example to show an extra explanation. This description is shown below the name.</p>
  </li>
  <li>
    <h4>Exclusivity</h4>
    <p>By enabling the <code>Exclusivity</code> feature you can make that choice exclusive, so if a respondent selects that choice, all other choices will be unselected, making the selected choice exclusive.</p>
  </li>
  <li>
    <h4>Labels</h4>
    <p>By default each choice will be marked as <code>Checked</code> or <code>Not checked</code> in your dataset (your results). By enabling the <code>Labels</code> feature you can overwrite these labels in your dataset with your own values for each choice (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information on labels over here</a>).</p>
  </li>
  <li>
    <h4>Identifier</h4>
    <p>By enabling the <code>Identifier</code> feature you can set an identifier for that choice to use in the dataset (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information on identifiers over here</a>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-checkboxes.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced options of the choices of the checkboxes block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The checkboxes block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Checkbox state (checked/unchecked per option);</li>
  <li>All checkboxes unchecked.</li>
</ul>
<h3 id="evaluate-conditions-block">Evaluate conditions</h3>
<p>Conditions for whole block:</p>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is specified;</li>
  <li>Value is not specified.</li>
</ul>
<p>Conditions for each option:</p>
<ul>
  <li>Option is true;</li>
  <li>Option is false;</li>
  <li>Option equals <code>your filter</code>;</li>
  <li>Option not equals <code>your filter</code>;</li>
  <li>Option is specified;</li>
  <li>Option is not specified.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
