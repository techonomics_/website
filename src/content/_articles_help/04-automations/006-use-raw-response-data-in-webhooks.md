---
layout: help-article
base: ../../../
permalink: /help/articles/use-raw-response-data-in-webhooks/
title: Use raw response data in webhooks (experts only) - Tripetto Help Center
description: In addition to the standard webhook connection, you can use the raw response data in your webhook. This does require additional actions and is for experts only.
article_title: Use raw response data in webhooks (experts only)
article_folder: automate-webhook-raw
author: jurgen
time: 5
category_id: automations
subcategory: automations_webhook
areas: [studio, wordpress]
---
<p>In addition to the standard webhook connection, you can use the raw response data in your webhook. This does require additional actions and is for experts only.</p>

<blockquote>
  <h4>Important! Experts only!</h4>
  <p>Please be aware the article below is for experts only. It does NOT describe how to use the standard, easier to use webhook connection.</p>
  <p><strong>If you just want to send response data to a webhook, please have a look at <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">this article</a>.</strong></p>
</blockquote>

<h2 id="when-to-use">When to use</h2>
<p>You can easily connect Tripetto to a webhook, as we described in <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">this article</a>. That standard connection sends the response data in such a way the webhook can understand the structure of the data to easily use it in your connections.</p>
<p>We always advise to follow that basic workflow, but in some cases you need more form data than just the questions and answers. For those cases it's also possible to use the raw response data. This will send way more data to the webhook, but also in a more complex JSON structure. That's why this is for experts only!</p>

<h2 id="how-to-use">How to use</h2>
<p>To use the raw response data you need to do some modifications in Tripetto, but most importantly, you need to configure your automation tool differently.</p>

<h3 id="enable-raw">Enable raw response data</h3>
<p>Let's start in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code>. The Automate pane will show up on the right side of the form builder.</p>
<p>We assume you already entered your webhook URL. Below that input field, you can now enable the option <code>Send raw response data to webhook</code>. From now on the webhook will no longer receive simple <code>Name-Value-Pairs</code>, but a <code>plain JSON response</code>.</p>

<h3 id="raw-hook">Catch raw webhook</h3>
<p>Now switch to your automation tool. Over there you need to <code>catch the raw webhook</code>, so the automation tool handles the raw data the right way. This is often a separate webhook event in your automation tool.</p>

<h3 id="manipulate">Manipulate raw data</h3>
<p>Now, it depends on what you want to do with the raw data. In some cases you will need to manipulate the data to make it usable for your follow-up actions.</p>
<p>One example of manipulating the data is making sure you can use the given answers. To do so, you need to execute a code snippet in your automation tool, so the data becomes usable.</p>
<p>It depends on the automation tool you're using how you can do this. For example in Zapier, you can easily add a JavaScript block and then for example enter the following code snippet to that block:</p>
```javascript
output = {};

var input = inputData.tripettoResult;

if (input.indexOf("\"") === 0) {
  input = JSON.parse(`{"data":${input}}`).data;
}

var tripettoFields = JSON.parse(input).fields;

for (var nField = 0; nField < tripettoFields.length; nField ++) {
   output["tripettoField" + nField] = tripettoFields[nField].string;
}
```
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/raw-zapier-script-update.png" alt="Screenshot of script in Zapier" />
  <figcaption>Example of a script block in Zapier.</figcaption>
</figure>

<h3 id="connect">Connect raw data to services</h3>
<p>Also the way you can connect the raw data to data fields in other services can be more difficult when you use the raw data. It depends on your automation tool and how you manipulate your raw response data how you can do that.</p>
<p>For example with the code snippet we added to Zapier in this article, your form structure gets available as JavaScript parameters that you can use in your follow-up service.</p>
