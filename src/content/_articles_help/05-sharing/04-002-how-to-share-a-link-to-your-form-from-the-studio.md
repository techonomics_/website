---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-share-a-link-to-your-form-from-the-studio/
title: How to share a link to your form from the studio - Tripetto Help Center
description: Learn how you can share your forms with a simple, shareable link.
article_title: How to share a link to your form from the studio
article_id: shareable-studio
article_folder: studio-share
author: jurgen
time: 1
category_id: sharing
subcategory: sharing_methods
areas: [studio]
---
<p>When your form structure is ready, you can use the shareable link to share your form/survey on social media, via email, or whatever way you want to reach your potential respondents.</p>

<h2 id="get-link">Get the shareable link</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there choose for method <code>Share a link</code> and you will see the dedicated shareable link. You can open the link immediately for yourself, or copy the URL to your clipboard to paste it somewhere else.
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-share.png" alt="Screenshot of sharing in Tripetto" />
  <figcaption>The Share screen in the studio.</figcaption>
</figure>

<h3 id="optimize">Optimize your sharing preview</h3>
<p>You can optimize your form for better sharing it across the web by adding a title, description and keywords to it. That information will be used inside previews when you share the link to your form. Please have a look at <a href="{{ page.base }}help/articles/how-to-optimize-your-form-for-better-sharing/" target="_blank">this article about optimizing your shareable link</a>.</p>

<blockquote>
  <h3>About your data</h3>
  <p>Both the form and collected data are stored under your account at Tripetto in Western Europe.</p>
  <p>If you want to take full control over your data, you should consider embedding your form. Have a look at <a href="{{ page.base }}help/articles/how-to-take-control-over-your-data-from-the-studio/" target="_blank">this article about taking control over your data from embedded forms</a>.</p>
</blockquote>
