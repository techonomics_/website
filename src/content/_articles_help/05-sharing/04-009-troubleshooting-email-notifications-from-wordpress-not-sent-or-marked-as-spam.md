---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/
title: Troubleshooting - Email notifications from WordPress not sent or marked as spam - Tripetto Help Center
description: If you use email notifications from your forms, it can happen those are not sent properly, or marked as spam. This article contains some troubleshooting for this.
article_title: Troubleshooting - Email notifications from WordPress not sent or marked as spam
author: jurgen
time: 2
category_id: sharing
subcategory: sharing_troubleshooting
areas: [wordpress]
---
<p>If you use email notifications from your forms, it can happen those are not sent properly, or marked as spam. This article contains some troubleshooting for this.</p>

<h2 id="plugin">Check your plugin version</h2>
<p>First of all, check which version of the Tripetto plugin you're on. You can find which version you're on the Plugins section in your WP Admin. Check our <a href="{{ page.base }}help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/" target="_blank">article on updating the plugin</a> if you're not on the latest release version.</p>

<h2 id="issues">Background of possible issues</h2>
<p>Tripetto offers a few possibilities to send mails:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-automate-email-notifications-for-each-new-result/" target="_blank">Automated email notification</a> of new entries;</li>
  <li>Emails from the form itself, created with the <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/" target="_blank">send email block</a>.</li>
</ul>
<p>As every WordPress instance is different (from server setup to plugins and themes), Tripetto follows the <a href="https://developer.wordpress.org/reference/functions/wp_mail/" target="_blank">WordPress standards regarding email functionality</a>: the <code>wp_mail()</code> function.</p>
<p>By default, the <code>wp_mail()</code> function of WordPress uses the <a href="https://github.com/PHPMailer/PHPMailer" target="_blank">PHPMailer function</a> to create and send unauthenticated emails from the webserver. This can result in delivery issues because it does not comply with current standards.</p>
<p>Issues with the default WordPress mailer function can be caused by:</p>
<ul>
  <li>An incorrect or non-existing WordPress mailer configuration;</li>
  <li>Your hosting platform not allowing the use of PHP for sending emails;</li>
  <li>Incorrect or non-existing DNS records, resulting in your webserver not being allowed to send emails on behalf of your domain;</li>
  <li>The use of an out-of-date or misconfigured plugin that replaces the default WordPress mailer function.</li>
</ul>

<h2 id="recommendations">Recommendations</h2>
<p>You can try the following to solve issues:</p>
<ul>
  <li>Make sure your WP installation is able to send mails using the default WordPress mailer function the correct way;</li>
  <li>Install, activate, and configure a plugin that replaces or extends the default WordPress mailer function. Most plugins use secure SMTP with authentication. Some plugins offer OAuth2.0 for authentication.</li>
  <li>Or use the WordPress API and extend the <code>wp_mail()</code> function to configure secure SMTP with authentication.</li>
</ul>

<blockquote>
<h4>Footnotes</h4>
  <p>Keep in mind that some solutions will store your email account details to either wp-config or the database, making it a potential risk when your website/web server gets compromised.</p>
  <p>We don’t recommend any specific plugins and don't provide support or instructions on how to use them.</p>
</blockquote>
<hr />

<h2>Still not working?</h2>
<p>Don't hesitate to <a href="{{ page.base }}contact/" target="_blank">contact us</a> if you have any problems with the WordPress plugin. <a href="javascript:$crisp.push(['do', 'chat:open']);">Using the chat</a> will be the fastest and easiest way to do so. We're happy to help you!</p>
