---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-embed-your-form-in-your-wordpress-site/
title: How to embed your form in your WordPress site - Tripetto Help Center
description: From the WordPress plugin you can embed your forms inside your WP site using the shortcode.
article_title: How to embed your form in your WordPress site
article_id: embed-wordpress
article_folder: wordpress-embed
author: mark
time: 4
category_id: sharing
subcategory: sharing_methods
areas: [wordpress]
---
<p>When your form structure is ready, you can embed your form on any position inside your WordPress site using the shortcode we provide.</p>

<h2 id="shortcodes">About shortcodes</h2>
<p>Shortcodes are a commonly used technique in WordPress to add snippets to your WordPress page. <a href="https://wordpress.com/support/shortcodes/" target="_blank">More information about shortcodes can be found at the website of WordPress</a>.</p>

<h2 id="get-shortcode">Get the Tripetto shortcode</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there you see the second method <code>WordPress shortcode</code> and you will see the basic syntax of the shortcode for WordPress sites.</p>
<figure>
  <img src="{{ page.base }}images/help/wordpress-share/00-wp-share.png" alt="Screenshot of embedding in Tripetto" />
  <figcaption>Open the Share screen.</figcaption>
</figure>

<h3 id="basic-shortcode">Basic shortcode</h3>
<p>The basic syntax is: <code>[tripetto id="#"]</code>, with the <code>id</code> parameter representing the id (integer) of the Tripetto form.</p>

<h3 id="customize-shortcode">Customize the shortcode</h3>
<p>Inside the Share screen you can customize the shortcode to determine the implementation of the form in your WP site. You can just toggle the desired settings and the shortcode will update automatically.</p>

<h3 id="parameters">Available settings</h3>
<p>The following settings are available:</p>
<ul>
  <li><h4>Allow pausing and resuming</h4>Setting if the form can be paused and resumed.<ul><li>Parameter in shortcode: <code>pausable</code> (optional)</li><li>Default value: <code>no</code></li><li>Possible values: <code>yes</code> | <code>no</code></li></ul></li>
  <li><h4>Save and restore uncompleted forms</h4>Setting for persisting data if users enter the form later on again.<ul><li>Parameter in shortcode: <code>persistent</code> (optional)</li><li>Default value: <code>no</code></li><li>Possible values: <code>yes</code> | <code>no</code></li></ul></li>
  <li><h4>Display form as full page overlay</h4>Setting to show the form as a full page overlay.<ul><li>Parameter in shortcode: <code>mode</code> (optional)</li><li>Default value: <code>inline</code></li><li>Possible values: <code>inline</code> | <code>page</code></li></ul></li>
  <li><h4>Specify a fixed width</h4>Setting to set the width of form container.<ul><li>Parameter in shortcode: <code>mode</code> (optional)</li><li>Default value: <code>100%</code></li><li>Possible values: <code>percentage value</code> + <code>%</code> | <code>pixel value</code> + <code>px</code></li></ul></li>
  <li><h4>Specify a fixed height</h4>Setting to set the height of form container.<ul><li>Parameter in shortcode: <code>height</code> (optional)</li><li>Default value: <code>auto</code></li><li>Possible values: <code>auto</code> | <code>percentage value</code> + <code>%</code> | <code>pixel value</code> + <code>px</code></li></ul></li>
  <li><h4>Display loading message</h4>Option to show a loading message to the form.<ul><li>Parameter in shortcode: <code>message</code> (optional)</li><li>Default value: <code>-</code></li><li>Value: <code>your loading message</code></li></ul></li>
  <li><h4>Disable asynchronous loading</h4>Option to disable asynchronous loading of your form to enhance the form loading time (please be aware that caching plugins may have effect on your Tripetto forms if you use this option).<ul><li>Parameter in shortcode: <code>async</code> (optional)</li><li>Default value: <code>-</code></li><li>Possible values: <code>no</code></li></ul></li>
  <li><h4>Specify custom CSS styles</h4>Option to add custom CSS (<a href="#custom-css" class="anchor">see instructions below</a>).<ul><li>Parameter in shortcode: <code>css</code> (optional)</li><li>Default value: <code>-</code></li><li>Value: <code>your custom css string</code></li></ul></li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-shortcode.gif" alt="Screenshot of the shortcode in Tripetto" />
  <figcaption>Customize the shortcode with the right settings.</figcaption>
</figure>

<h3 id="copy-shortcode">Copy shortcode</h3>
<p>When you're done customizing the shortcode, you can copy the shortcode to your clipboard to use it in your WP site.</p>

<h2 id="use-shortcode">Use the shortcode</h2>
<p>Now that you've got your shortcode, navigate to the position inside your WP Admin where you want to embed the Tripetto form. In that position add a shortcode block: Click the <code><i class="fas fa-plus"></i></code> icon <i class="fas fa-arrow-right"></i>Click <code>Shortcode</code>. A shortcode block will be inserted. In that block you can now paste the shortcode of the Tripetto form. You can now save the draft and test it.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-page.png" alt="Screenshot of the shortcode in WordPress" />
  <figcaption>Insert the shortcode in a page.</figcaption>
</figure>

<h2 id="custom-css">Custom CSS</h2>
<p>It's possible to add custom CSS to parts of your embedded form. Please first have a look at the <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">built-in advanced styling options</a> to see if that's sufficient for your styling purposes.</p>
<blockquote>
  <h4>Disclaimer for using custom CSS</h4>
  <p>The usage of custom CSS is totally at own risk. We don't offer support on the usage of custom CSS or forms that include custom CSS.</p>
</blockquote>
<p>Custom CSS can only manipulate CSS inside (question) blocks of your form, using the <code>css</code> parameter inside your shortcode. You can address a certain block by using the <code>data-block</code> selector and referring to the desired block name. You can then add the custom CSS for that block as a string value.</p>
<p>We advise to always use the shortcode editor to add your custom CSS. For example, if you want to manipulate the color of the title of the text input blocks (referred to by <code>tripetto-block-text</code>), add this to the custom CSS option in the shortcode editor:</p>
```typescript
[data-block='tripetto-block-text'] { h2 { color: blue; } }
```

<h2 id="data" data-anchor="Data control">Data control in the WordPress plugin</h2>
<p>Both the form and collected data are hosted and stored in your own WordPress instance. Not a single connection related to Tripetto is ever made with an external host other than yours. It's all in your own instance, under your control.</p>
