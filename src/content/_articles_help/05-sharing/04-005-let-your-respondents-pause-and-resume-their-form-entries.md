---
layout: help-article
base: ../../../
permalink: /help/articles/let-your-respondents-pause-and-resume-their-form-entries/
title: Let your respondents pause and resume their form entries - Tripetto Help Center
description: Yes, respondents to your Tripetto forms can pause and resume their form entries, so they can continue later on. Especially handy with large forms.
article_title: Let your respondents pause and resume their form entries
author: mark
time: 2
category_id: sharing
subcategory: sharing_uncompleted
areas: [studio, wordpress]
---
<p>Yes, respondents to your Tripetto forms can pause and resume their form entries, so they can continue later on. Especially handy with large forms.</p>

<h2 id="how-it-works">How it works</h2>
<p>This is how it works:</p>
<ul>
  <li>
    <h3 id="pause">Pause</h3>
    <p>If the pause and resume functionality is enabled a <code>Pause</code> button will be shown inside the form. If the respondent clicks that button, an email address can be entered. This email address will be used to send the resume link to. The pause form will even try to fill out the email address itself, based on any already filled out questions with question type 'Email address'.</p>
  </li>
  <li>
    <h3 id="email">Email message</h3>
    <p>After submitting the pause form, the resume link will be sent to the entered email address.</p>
  </li>
  <li>
    <h3 id="resume">Resume</h3>
    <p>The resume link in the email takes the respondent back to the position where the form was paused, including all given answers until that point.</p>
  </li>
</ul>

<h2 id="shareable-links" data-anchor="In shareable links">Pause and resume in shareable links</h2>
<p>If your respondents use the form from a shareable link, the pause and resume functionality works automatically.</p>

<h2 id="embed" data-anchor="In embedded forms">Pause and resume in embedded forms</h2>
<p>If you choose to embed your form, you can also use the pause and resume functionality, but you have to manually activate it inside the embed code.</p>
