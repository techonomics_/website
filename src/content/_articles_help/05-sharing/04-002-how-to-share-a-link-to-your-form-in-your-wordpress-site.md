---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-share-a-link-to-your-form-in-your-wordpress-site/
title: How to share a link to your form in your WordPress site - Tripetto Help Center
description: Learn how you can share your forms in your WordPress site with a simple, shareable link.
article_title: How to share a link to your form in your WordPress site
article_id: shareable-wordpress
author: jurgen
time: 1
category_id: sharing
subcategory: sharing_methods
areas: [wordpress]
---
<p>When your form structure is ready, you can use the shareable link to share your form/survey on social media, via email, or whatever way you want to reach your potential respondents.</p>

<h2 id="get-link">Get the shareable link</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there you see the first method <code>Shareable link</code> and you will see the URL of the shareable link. You can open the link immediately for yourself, or copy the URL to your clipboard to paste it somewhere else.
<figure>
  <img src="{{ page.base }}images/help/wordpress-share/00-wp-share.png" alt="Screenshot of sharing in Tripetto" />
  <figcaption>Open or copy the shareable link.</figcaption>
</figure>

<h2 id="data" data-anchor="Data control">Data control in the WordPress plugin</h2>
<p>Both the form and collected data are hosted and stored in your own WordPress instance. Not a single connection related to Tripetto is ever made with an external host other than yours. It's all in your own instance, under your control.</p>
