---
layout: help-article
base: ../../../
permalink: /help/articles/how-tripetto-prevents-spam-entries-from-your-forms/
title: How Tripetto prevents spam entries from your forms - Tripetto Help Center
description: You like spam? We neither! That's why Tripetto forms work quite differently in the core than other forms, preventing spam entries automatically.
article_title: How Tripetto prevents spam entries from your forms
article_folder: spam
author: mark
time: 3
category_id: sharing
subcategory: sharing_spam
areas: [studio, wordpress]
---
<p>You like spam? We neither! That's why Tripetto forms work quite differently in the core than other forms, preventing spam entries from within.</p>

<h2 id="others">Spam entries in simple forms</h2>
<p>It's an often heard complaint that online forms can be accessed by spammers (like spambots), resulting in lots of unwanted and unpleasant form entries. The main reason for that is it's quite easy for spammers to understand the content and structure of simple online forms. As soon as a spammer knows this, it can start posting spam entries automatically.</p>
<h3>CAPTCHAs 🔣 Traffic signs 🚦 I'm no robot 🤖</h3>
<p>To prevent spam entries you often see 'solutions' that are difficult for spammers to fill out, like decoding unreadable CAPTCHAs, identifying traffic signs or validating you're no robot.</p>
<p>Those solutions technically may work to prevent most spam entries, but it also works very well for something else: a highly decreased response rate! Nobody likes to fill out those questions and often it takes longer to solve the puzzle than to fill out the form itself. Not very good for your user's experience, nor your response rate.</p>
<p>As you may have noticed, we at Tripetto are not big fans of all this...</p>
<hr />

<h2 id="tripetto">Tripetto vs spam entries</h2>
<p>Luckily Tripetto is not just a simple form. It's built up from the core in such a way it's very hard for spammers to use the form. It uses the following principles to take care of that:</p>
<ul>
  <li><strong>Data structure difficulty</strong> - The technical structure of your forms is managed in such a way spammers will have a very hard time learning the structure of the form. And without understanding the structure, it gets hard for the spammer to even use that structure to fill out the input fields, preventing it from being able to fill out the form automatically;</li>
  <li><strong>Increasing posting difficulty</strong> - Next to the difficulty of filling out the form, we also make it very hard for spammers to submit Tripetto forms. There is a sophisticated technique behind the submitting process that increases in difficulty each time a spammer tries to submit a form. A concept we borrowed from blockchain technology. This will discourage spammers to repeatedly/automatically keep submitting forms.</li>
</ul>
<p>Of course your real respondents won't have any notice of these built in difficulties.</p>
<hr />
<p>So instead of bothering your respondents with spam protection, we just make sure everything under the hood is safe, without bothering anyone with it.</p>
<p>But of course you can still add a picture of traffic lights to your form if you'd like to... 😉</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-traffic-lights.jpg" alt="Photo illustrating traffic lights" />
  <figcaption>Please click the traffic lights. Nahh, not with Tripetto forms.<br/><a href="https://unsplash.com/photos/PUk3xdOwsX8" target="_blank">Photo credits Unsplash</a>.</figcaption>
</figure>
