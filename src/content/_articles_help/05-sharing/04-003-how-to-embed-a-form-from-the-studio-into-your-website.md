---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-embed-a-form-from-the-studio-into-your-website/
title: How to embed a form from the studio into your website - Tripetto Help Center
description: Learn how to embed a form from the studio into your own website.
article_title: How to embed a form from the studio into your website
article_id: embed-studio
article_folder: studio-embed
author: mark
time: 3
category_id: sharing
subcategory: sharing_methods
areas: [studio]
---
<p>You can share your forms with a <a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-from-the-studio/" target="_blank">simple link</a>, but you can also embed the form in your own website. Additionally to this, you can have full control over the collected data (<a href="{{ page.base }}help/articles/how-to-take-control-over-your-data-from-the-studio/" target="_blank">read more about taking control over your data here</a>).</p>

<h2 id="embed-get">Get the embed code</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there choose for method <code>Embed in a website or application</code> and you will see the embed code at the bottom of the Share pane.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-embed.png" alt="Screenshot of embedding in Tripetto" />
  <figcaption>Choose for embedding your form.</figcaption>
</figure>

<p>Let's first have a look at the available embed types, before we explain the more advanced hosting options.</p>

<h3 id="types">Embed types</h3>
<p>We provide multiple embed codes to let your easily embed the form the way you want. You can select the desired method by selecting one of the following options in the <code>Embed type</code> dropdown:</p>
<ul>
  <li>Select <code>HTML <i class="fas fa-arrow-right"></i> Snippet (inline with other content)</code> - The form gets placed inline on the position you place the embed code in your code;</li>
  <li>Select <code>HTML <i class="fas fa-arrow-right"></i> Snippet (whole page)</code> - The form covers the whole page. You can place the snippet at the desired position in your code;</li>
  <li>Select <code>HTML <i class="fas fa-arrow-right"></i> Page</code> - A full working page example with the form covering the whole page;</li>
  <li>Select <code>Embed in application <i class="fas fa-arrow-right"></i> JavaScript (ES6) / TypeScript</code> - The form gets embedded using ES6 imports on the position you place the embed code in your code. Make sure you also install the required npm packages (command also included in the studio);</li>
  <li>Select <code>Go commando <i class="fas fa-arrow-right"></i> Tripetto Form Kit</code> - Build your own form runner, or even integrate our form builder in your own software using the SDK. We provide the form definition of the particular form you were working on. More info about the SDK can be found over <a href="{{ page.base }}developers/" target="_blank">here</a>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-code.png" alt="Screenshot of embedding in Tripetto" />
  <figcaption>Select the embed type and get the embed code.</figcaption>
</figure>

<h4>Attach your form</h4>
<p>Please notice you can attach your form to any element you'd like inside your page. Most of our embed codes attach the form to the body of the page, but you can control the <code>element</code> parameter inside the embed code. You can use that parameter to attach your form to any desired element.</p>
<p>Use the embed type <code>Snippet (inline with other content)</code> to easily attach the form to an HTML element (that element is already in the snippet).</p>

<h4>That's it!</h4>
<p>In most cases, this is it! After you've selected the right embed type, you can copy the whole embed code to your clipboard so you can paste it inside your code immediately.</p>
<hr />

<h3 id="hosting">Hosting options</h3>
<p>By using the embed code, you can also have full control over the data of your form. That way you can for example use the benefits of the Tripetto platform, but save the collected data only to your own back office, without it ever being saved to the Tripetto infrastructure. This can be very important with regards to <a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/" target="_blank">privacy legislation, like GDPR</a>.</p>
<p>Please have a look at <a href="{{ page.base }}help/articles/how-to-take-control-over-your-data-from-the-studio/" target="_blank">this specific article for more information on full data control from your embedded forms</a>.</p>
<hr />

<h3 id="custom-css">Custom CSS options</h3>
<p>It's even possible to add custom CSS to parts of your embedded form. Please first have a look at the <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">built-in advanced styling options</a> to see if that's sufficient for your styling purposes.</p>
<blockquote>
  <h4>Disclaimer for using custom CSS</h4>
  <p>The usage of custom CSS is totally at own risk. We don't offer support on the usage of custom CSS or forms that include custom CSS.</p>
</blockquote>
<p>Custom CSS can only manipulate CSS inside (question) blocks of your form, using the <code>customCSS</code> parameter inside your embed code. You can address a certain block by using the <code>data-block</code> selector and referring to the desired block name. You can then add the custom CSS for that block as a string value.</p>
<p>For example, if you want to manipulate the color of the title of the text input blocks (referred to by <code>tripetto-block-text</code>), add this to your embed code:</p>
```typescript
customCSS: "[data-block='tripetto-block-text'] { h2 { color: blue; } }"
```
