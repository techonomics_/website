---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-raise-error-block/
title: How to use the raise error block - Tripetto Help Center
description: Use the raise error block if you want to prevent a form from submitting, for example when a certain condition is (not) matched.
article_title: How to use the raise error block
article_id: action-blocks
article_folder: editor-block-raise-error
author: mark
time: 3
category_id: logic
subcategory: logic_error
areas: [studio, wordpress]
---
<p>Use a raise error block if you want to prevent a form from submitting, for example when a certain condition is (not) matched.</p>

<h2 id="when-to-use">When to use</h2>
<p>In general the raise error blocks shows an error message in your form without the possibility to proceed and submit the form. Why would you want that?!</p>
<p>Your forms in Tripetto of course automatically validate inputs of respondents. For example a required field that's not filled out will be marked with an error and preventing the form from being submitted.</p>
<p>But we can imagine more complicated situations where the form cannot validate automatically.</p>
<p>For example you could have a survey in which your first question is a <a href="{{ page.base }}help/articles/how-to-use-the-yes-no-block/" target="_blank">yes/no question</a> to determine if the respondent meets your requirements. If the respondent answers 'No', you don't want the form to be submitted, as it will provide not valuable data to your responses. So in that case you can raise a custom error message to let those respondents know this survey isn't meant for them.</p>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The hidden field block is available as a question type. Add a new block to your form and then select the question type <code>Raise error</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-add.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Raise error blocks are available from the Question type menu.</figcaption>
</figure>
<blockquote>
  <h4>Tip</h4>
  <p>Of course make sure you only add a raise error block on the right places in your form's structure, as it prevents a respondent from finishing your form.</p>
</blockquote>

<h3 id="basic-features">Basic features</h3>
<p>You can now add the error message you'd like to show to your respondents. The error message will be shown as a common block inside the form, but without any interaction visible, preventing the respondent to finish the form.</p>
<p>The following fields are available for the error:</p>
<ul>
  <li>
    <h4>Error message</h4>
    <p>The main title of the error message.</p>
  </li>
  <li>
    <h4>Description</h4>
    <p>An additional description in the error message.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-error.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a raise error block, placed inside a branch if the respondent answers 'No' to the first question.</figcaption>
</figure>
<hr />

{% include help-article-blocks.html %}
