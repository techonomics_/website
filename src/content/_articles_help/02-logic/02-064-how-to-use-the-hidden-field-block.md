---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-hidden-field-block/
title: How to use the hidden field block - Tripetto Help Center
description: Use hidden fields to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.
article_title: How to use the hidden field block
article_id: action-blocks
article_folder: editor-block-hidden-field
author: mark
time: 5
category_id: logic
subcategory: logic_hidden
areas: [studio, wordpress]
---
<p>Hidden fields are like hidden gems. Use them to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.</p>

<h2 id="when-to-use">When to use</h2>
<p>The idea of a hidden field block is you're able to store information without a visual representation in the form. That information is then saved with the rest of the form data when the form is submitted. But it can also be used to create logic in your form and make certain decisions based on the information in the hidden field.</p>

<h2 id="how-to-use">How to use</h2>
<p>The hidden field block is available as a question type. Add a new block to your form and then select the question type <code>Hidden field</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-add.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Hidden fields are available from the Question type menu.</figcaption>
</figure>

<h3 id="basic-features">Basic features</h3>
<p>You can now change the features of the block to get the desired behavior:</p>
<ul>
  <li>
    <h4>Name</h4>
    <p>The name of the field in your form (not visible for respondents).</p>
  </li>
  <li>
    <h4>Type of field</h4>
    <p>This determines the type of information you want to get. Examples of field types are a querystring, a cookie, a page URL, a timestamp and browser information (like the user language).</p>
  </li>
</ul>
<h3 id="additional-features">Additional features</h3>
<p>Depending on the selected type of field, additional features and settings can become activated to help you get the right value in your hidden field.</p>
<hr/>

<h2 id="logic">Logic</h2>
<p>After you retrieved a hidden field in your form, you can instantly use that value to execute logic based on the value of the hidden field. For example, you can determine the flow in your form depending on the browser language of the respondent.</p>
<p>This works the same way you can add logic based on 'normal' question blocks, like you can see in <a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank">this article on logic</a>. It depends on the type of hidden field block which branch conditions are available.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-logic.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Use a hidden field value to execute logic.</figcaption>
</figure>

<h2 id="logic-numeric">Logic (numeric hidden fields)</h2>
<p>If your hidden field outputs a numeric value, it can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a>:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Value is equal to <code>your filter</code>;</li>
  <li>Value is not equal to <code>your filter</code>;</li>
  <li>Value is lower than<code>your filter</code>;</li>
  <li>Value is higher than <code>your filter</code>;</li>
  <li>Value is between <code>your filters</code>;</li>
  <li>Value is not between <code>your filters</code>;</li>
  <li>Value is specified;</li>
  <li>Value is not specified.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<p>Similar to block conditions.</p>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>

<h2 id="logic-text">Logic (textual hidden fields)</h2>
<p>If your hidden field outputs a textual value, it can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a>:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is specified;</li>
  <li>Value is not specified.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<p>Similar to block conditions.</p>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

<h2 id="examples">Examples</h2>
<p>Let us show you some examples of how to use the hidden field.</p>

<h3 id="example_page_url">Get page URL</h3>
<p>Let's say you have embedded a Tripetto form on multiple pages inside your website and you want to know from which page someone has submitted a form. The hidden field can simply gather the page URL of the page that the form is embedded in and save that data to the entry data.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-page-url.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a hidden field getting the page URL.</figcaption>
</figure>

<h3 id="example_querystring">Get value from query string</h3>
<p>The URL of a form/page can also contain certain values. Those are often stored in the <strong>query string</strong> of the URL. The query string is at the end of the URL and can contain fully flexible <strong>name-value pairs</strong>.</p>
<blockquote>
  <h4>About querystrings</h4>
  <p>A querystring is a somewhat technical tool to send data in a URL. Some background information on how to supply a querystring:</p>
  <ul>
    <li>You can add multiple parameters to a querystring;</li>
    <li>Each parameter consists of a name and a value, separated by a <code> = </code> sign, for example <code>name=value</code>;</li>
    <li>The first parameter is preceded by a <code> ? </code> sign;</li>
    <li>Any following parameters are preceded by a <code> & </code> sign;</li>
  </ul>
  <p>A full example could be something like this: <code>https://yoursite.com/?name=abc&city=klm&country=xyz</code>.</p>
</blockquote>
<p>Let's say we have a form that we share via different channels, for example a mailing and Twitter. And you want to know how someone entered the form, so you can analyse which channel worked best. By adding a query string named <code>referral</code> to your URL's, you can add various values for that query string parameter that can be saved in a hidden field in the form.</p>
<p>So in the URL you share via a mailing the query string in the URL is something like <code>?referral=mailing</code>. And for the URL you share on Twitter something like <code>?referral=twitter</code>. Notice that all parameter names and values we use are just examples; those are totally flexible for your own needs.</p>
<p>Now, by adding a hidden field that reads the query string parameter <code>referral</code>, you can save where someone came from. Each entry will now contain where someone came from: the mailing, or Twitter.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-querystring.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a hidden field getting the query string parameter <code>referral</code>.</figcaption>
</figure>
<hr/>

<h2>Also read our blog</h2>
<p>We also wrote a blog post with some more background information, use cases and working examples of hidden fields.</p>
<div>
  <a href="{{ page.base }}blog/hidden-fields-in-tripetto/" target="_blank" class="blocklink">
    <div>
      <span class="title">Hidden fields in Tripetto - Tripetto Blog<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Hidden fields are like hidden gems. Use them to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<hr />

{% include help-article-blocks.html %}
