---
layout: help-article
base: ../../../
permalink: /help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/
title: Learn about different types of branch conditions for your logic - Tripetto Help Center
description: Tripetto offers several types of branch conditions to add the desired smartness to your forms.
article_title: Learn about different types of branch conditions for your logic
article_id: logic-details
article_folder: editor-branch-conditions
author: jurgen
time: 5
time_video: 14
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>Tripetto offers several types of branch conditions to add the desired logic and smartness to your forms. In this article we explain the different types to do so.</p>

<h2 id="builder">Branches</h2>
<p>In Tripetto you use branches to add the desired logic to your form. In <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">this article we showed how to add branches to your form</a>.</p>
<p>Part of each branch are the <strong>branch conditions</strong> that determine when a certain branch is executed. Let's take a deeper look into that.</p>

<h2 id="types">Branch conditions</h2>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/05-add.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Adding multiple branch conditions.</figcaption>
</figure>
<p>To determine when a certain branch should be shown to your respondents you have to add the desired <strong>branch condition(s)</strong> to each branch. To do so, you click the <code><i class="fas fa-plus"></i></code> button at the bottom of the branch block. Now you can use several types of branch conditions, which we'll describe in detail below:
  <ul>
    <li>Block conditions;</li>
    <li>Evaluate conditions;</li>
    <li>Regular expression conditions;</li>
    <li>Device conditions.</li>
  </ul>
</p>
<p>You can use all condition types totally flexible within each branch, so you can also make combinations of different types of conditions and then let <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/" target="_blank">the behavior of the branch</a> decide when the branch should be followed.</p>
<p>In that way you can create <a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank">simple branches with one condition like this example</a>, but you can also make <a href="{{ page.base }}help/articles/how-to-match-a-combination-of-multiple-conditions/" target="_blank">combinations of conditions like in this article</a>.</p>

<h3 id="block-conditions">Block conditions</h3>
<p>In most cases the block conditions will be sufficient. You use them to validate if the given answer of a certain question block matches your wish(es).</p>
<p>Depending on the question type, you have several options to check the value of each condition. In general you can choose between:</p>
<ul>
  <li><code>Fixed value</code> - A fixed value that you enter in the form builder;</li>
  <li><code>Answered value</code> - A flexible value that your respondent has answered in another question in your form.</li>
</ul>
<p>Some examples of block conditions:</p>
<ul>
  <li>If the respondent selects the <i>'Other...'</i> option at the dropdown question <i>'Question 1'</i>, then show this branch (<a href="{{ page.base }}help/articles/how-to-create-an-other-option/" target="_blank">also see this help article</a>);</li>
  <li>If the respondent gives a rating of <i>'5 stars'</i> at the rating question <i>'Question 2'</i>, then show this branch (<a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank">also see this help article</a>);</li>
  <li>If the respondent answers <i>'Option 1'</i> and/or <i>'Option 2'</i> at the multiple choice question <i>'Question 3'</i>, then iterate through this branch for each selected option (<a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-multiple-selected-options/" target="_blank">also see this help article</a>);</li>
  <li>If the respondent enters the right password for the password question <i>'Question 4'</i>, then show this branch (<a href="{{ page.base }}help/articles/how-to-verify-passwords-inside-your-form/" target="_blank">also see this help article</a>).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-basic.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a branch with a block condition.</figcaption>
</figure>

<h3 id="evaluate-conditions">Evaluate conditions</h3>
<p>For more advanced conditions based on the respondent's answers, you can use the evaluate condition. Just like the block conditions, you use them to validate if the given answer of a certain question matches your wish(es), but then with some more advanced filters.</p>
<p>Depending on the question type, you have several options to check the value of each condition. In general you can choose between:</p>
<ul>
  <li><code>Fixed value</code> - A fixed value that you enter in the form builder;</li>
  <li><code>Answered value</code> - A flexible value that your respondent has answered in another question in your form.</li>
</ul>
<p>Some examples of evaluate conditions:</p>
<ul>
  <li>If the respondent does not select <i>'Option A'</i> at the dropdown question <i>'Question 5'</i>, then show this branch;</li>
  <li>If the respondent selects a date in the date question <i>'Question 6'</i> that's between <i>'January 1, 2020'</i> and <i>'January 31, 2020'</i>, then show this branch;</li>
  <li>If the respondent gives a rating of <i>'3 stars or higher'</i> at the rating question <i>'Question 7'</i>, then show this branch;</li>
  <li>If the respondent enters an email address that contains <i>'@gmail.com'</i> at the email address question <i>'Question 8'</i>, then show this branch;</li>
  <li>If the respondent does not answer 'Statement 1' of the matrix question <i>'Question 9'</i>, then show this branch.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-evaluate.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a branch with an evaluate condition, with the condition settings opened.</figcaption>
</figure>

<h3 id="regular-expression">Regular expression conditions</h3>
<p>Regular expression are a bit more complicated, but very powerful to check if an answered value matches a certain desired format. You declare the desired format in a <a href="https://en.wikipedia.org/wiki/Regular_expression" target="_blank">regular expression</a>. For example:</p>
<ul>
  <li>If the respondent enters a value that starts with <i>'hi'</i> at the text question <i>'Question 9'</i>, then show this branch (regular expression used for this: <code>/^hi/</code>);</li>
  <li>If the respondent enters a value that's exactly 8 uppercase characters (for example a discount code) at the text question <i>'Question 10'</i>, then show this branch (regular expression used for this: <code>/^[A-Z]{8}$/</code>);</li>
  <li>If the respondent enters a value that's exactly 16 digits (for example a social security number) at the text question <i>'Question 11'</i>, then show this branch (regular expression used for this: <code>/^[0-9]{16}$/</code>).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-regex.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a branch with a regular expression condition, with the condition settings opened.</figcaption>
</figure>
<blockquote>
  <h4>Tip</h4>
  <p>You can use <a href="https://regex101.com/" target="_blank">regex101.com</a> to create and test your regular expressions.</p>
</blockquote>

<h3 id="device-conditions">Device conditions</h3>
<p>With device conditions you can show/hide certain parts of your form to users of a certain device (based on the screen size). For example:</p>
<ul>
  <li>If the respondent is using a <i>'Phone (small screen device)'</i>, then show this branch;</li>
  <li>If the respondent is using a <i>'Desktop (large screen device)'</i>, then show this branch;</li>
  <li>If the respondent is using a <i>'Phone (small screen device) or Tablet (medium screen device)'</i>, then show this branch.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-device.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of two branches with different device conditions.</figcaption>
</figure>
<hr />

<h2 id="tutorials">Video tutorials</h2>
<p>We have made some video tutorials on how to use branches. It shows where to do this in the form builder and what options you have.</p>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/pb01iB9UhDQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on simple logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/RfD9bXK9Sos" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on branch logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/DgyYh_xPWuo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on advanced logic.</figcaption>
</figure>
