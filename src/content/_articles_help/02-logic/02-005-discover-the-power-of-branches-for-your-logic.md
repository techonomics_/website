---
layout: help-article
base: ../../../
permalink: /help/articles/discover-the-power-of-branches-for-your-logic/
title: Discover the power of branches for your logic - Tripetto Help Center
description: The way Tripetto lets you create branches for your logic is so flexible the possibilities are limitless. Let us show you how to get the most out of it.
article_title: Discover the power of branches for your logic
article_id: logic-basics
author: jurgen
time: 4
time_video: 14
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
popular: true
---
<p>The way Tripetto lets you create branches for your logic is so flexible the possibilities are limitless. Let us show you how to get the most out of it.</p>

<h2 id="builder" data-anchor="Visual form builder">Logic in our visual form builder</h2>
<p>Tripetto is built up from the base to support advanced logic options to make your forms really smart. To easily let you create (and maintain) that logic, you create your forms in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">our visual form builder</a> that representents your form's flow on a storyboard.</p>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-branch/00-add.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Add a new branch.</figcaption>
</figure>

<h2 id="branches">Branches</h2>
<p>To add the magic of logic to it, you create <strong>branches</strong>, that are shown as a new horizontal track in the form builder. Inside each branch you can add unlimited clusters, question blocks, action blocks or even more branches. There are several ways to create a branch, but for this article we create an empty branch by clicking the <code><i class="fas fa-plus"></i></code> icon on the right side of a cluster.</p>

<h3 id="branch-conditions">Branch conditions</h3>
<p>To determine when a certain branch should be shown to your respondents you have to add the desired <strong>branch condition(s)</strong> to each branch. To do so, you click the <code><i class="fas fa-plus"></i></code> button at the bottom of the branch block. Now you can use several branch conditions:</p>
<ul>
  <li><strong>Block conditions</strong> - Basic conditions based on the value of a certain question block and/or action block;</li>
  <li><strong>Evaluate conditions</strong> - Advanced conditions based on the value of a certain question block and/or action block;</li>
  <li><strong>Regular expression conditions</strong> - Conditions based on a regular expression;</li>
  <li><strong>Device conditions</strong> - Conditions based on the device of the respondent.</li>
</ul>
<p>Please have a look at <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">this article for more in depth information about branch conditions</a>.</p>

<h3 id="branch-behavior">Branch behavior</h3>
<p>To determine how a branch should behave when one or more conditions match, you can select the right <strong>branch behavior</strong>. To do so, you click the green bubble at the top of a branch. Now you can choose from different branch behaviors:</p>
<ul>
  <li><strong>For the first condition match</strong> - Follow the branch if <i>at least one</i> of the conditions matches;</li>
  <li><strong>When all conditions match</strong> - Follow the branch if <i>all</i> the conditions match;</li>
  <li><strong>For each condition match (iteration)</strong> - Follow the branch for <i>each</i> condition match. This will create an iteration of the branch.</li>
</ul>
<p>Please have a look at <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/" target="_blank">this article for more in depth information about branch behaviors</a>.</p>

<h3 id="branch-ending">Branch ending</h3>
<p>To determine what should happen at the end of a branch, you can set the desired <strong>branch ending</strong>. To do so, you click the green bubble at the bottom of a branch. Now you can choose from different branch endings:</p>
<ul>
  <li><strong>Continue with the next cluster or branch</strong> - Let the form determine the continuation;</li>
  <li><strong>Jump to a specific point</strong> - Let the form jump to a specific cluster in your form;</li>
  <li><strong>Jump to end</strong> - Let the form jump to the very end immediately;</li>
  <li><strong>End with closing message</strong> - Let the form end and show a custom closing message.</li>
</ul>
<p>Please have a look at <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-endings-for-your-logic/" target="_blank">this article for more in depth information about branch endings</a>.</p>

<h3 id="branch-properties">Branch properties</h3>
<p>Instead of using the bubbles at the beginning (to set branch behavior) and ending (to set branch ending) of a branch, you can also click on the name of the branch to open up the branch properties. In the pane that opens you can set the branch name, branch behavior and branch ending.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch/01-properties.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Open up the branch properties.</figcaption>
</figure>
<hr />

<h2 id="tutorials">Video tutorials</h2>
<p>We have made some video tutorials on how to use branches. It shows where to do this in the form builder and what options you have.</p>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/pb01iB9UhDQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on simple logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/RfD9bXK9Sos" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on branch logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/DgyYh_xPWuo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on advanced logic.</figcaption>
</figure>
