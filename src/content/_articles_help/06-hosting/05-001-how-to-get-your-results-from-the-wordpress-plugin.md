---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-get-your-results-from-the-wordpress-plugin/
title: How to get your results from the WordPress plugin - Tripetto Help Center
description: Learn how you can manage and export your results to CSV to use in Excel or Google Sheets.
article_title: How to get your results from the WordPress plugin
article_id: download-wordpress
article_folder: wordpress-results
author: jurgen
time: 2
category_id: hosting
subcategory: hosting_results
areas: [wordpress]
---
<p>Learn how you can manage and export your results to CSV so you can use your data for example in Excel or Google Sheets.</p>

<h2 id="manage">Manage your results</h2>
<p>Inside the WordPress plugin you can see and manage all results of each form. From the list of your forms (also see <a href="{{ page.base }}help/articles/how-to-add-and-manage-forms-in-the-wordpress-plugin/" target="_blank">this article</a>) you can click <code>Results</code> by moving your cursor over each row in the list.</p>
<p>You can also click <code>Results</code> at the top menu of the form builder while editing a certain form.</p>

<h3 id="view">View results</h3>
<p>This will show a list of all results you received for the particular form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-list.png" alt="Screenshot of results in Tripetto" />
  <figcaption>View list of entries.</figcaption>
</figure>

<p>You can click each result to view the data of the result right away.</p>
<p>If you have one or multiple File upload blocks in your form, the uploaded files from your respondents are available as individual download links in the result view.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-details.png" alt="Screenshot of results in Tripetto" />
  <figcaption>View the details of each response.</figcaption>
</figure>

<h3 id="delete">Delete results</h3>
<p>From the list of results, you can delete one or multiple result(s) that you don't longer need. With the checkboxes in front of each row you can select the results you want to delete. After the selection, you can collapse the <code>Bulk ations</code> dropdown <i class="fas fa-arrow-right"></i>Select<code> Delete</code><i class="fas fa-arrow-right"></i>Click <code>Apply</code>. This will permanently delete the selected results (after confirmation).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-delete.png" alt="Screenshot of results in Tripetto" />
  <figcaption>Select multiple responses to delete them.</figcaption>
</figure>

<h2 id="export">Export your results</h2>
<p>Exporting your results is very easy. From the top of the 'Entries' screen just click <code>Export entries as CSV</code>. The download of your results will start immediately and will be downloaded as a CSV file to your Downloads folder.</p>
<p>If you have one or multiple File upload blocks in your form, the uploaded files from your respondents are available as individual download links in the CSV export.</p>

<blockquote>
  <h3 id="multiple">Seeing multiple download buttons?</h3>
  <p>It can be possible you see multiple download buttons, grouped on date. Most probably this means you have results of different form definitions. This can occur when you change the structure of your form, resulting in a changed fieldset of your form data. Read <a href="{{ page.base }}help/articles/troubleshooting-seeing-multiple-download-buttons/" target="_blank">this article</a> for more information on that.</p>
</blockquote>

<h2 id="use">Use your results</h2>
<p>If you have downloaded the CSV file you can do what you'd like with the data. For example, open the CSV file in a spreadsheet editor, like Microsoft Excel or Google Sheets. In there you can convert the comma-separated values (CSV) to columns. More information on how to do that can be found in <a href="{{ page.base }}help/articles/how-to-use-your-csv-file-in-office-excel-or-google-sheets/" target="_blank">this article</a>.</p>
<p>After that, you can process your data, for example, by calculating averages or displaying the data in graphs. How to do that, depends on the spreadsheet editor you are using.</p>
<p>Another option to use your results is by connecting your form with a webhook to automate all kinds of actions. More information on how to do that can be found in <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/" target="_blank">this article</a>.</p>

<h2 id="notifications">Get notifications by email and Slack</h2>
<p>You can also get notifications of new responses by email and/or in Slack. You can include all form data directly in those notifications. Please see the following articles for more information:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-automate-email-notifications-for-each-new-result/" target="_blank">How to get email notifications for each new result</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-automate-slack-notifications-for-each-new-result/" target="_blank">How to get Slack notifications for each new result</a>.</li>
</ul>
