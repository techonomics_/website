---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-and-manage-forms-in-the-studio/
title: How to add and manage forms in the studio - Tripetto Help Center
description: Learn how to add and manage forms in the studio using workspaces.
article_title: How to add and manage forms in the studio
article_folder: studio-manage-forms
author: martijn
time: 2
time_video: 0
category_id: basics
areas: [studio]
---
<p>You can create an unlimited amount of forms in the studio, but how do you keep these forms organized? Let's find out.</p>

<h2 id="create">Create new form</h2>
<p>After you <a href="{{ page.base }}help/articles/how-to-create-an-account-in-the-studio/" target="_blank">created your account</a>, you can start a new form in your Workspace. To do so, first let us explain how the organization of forms work.</p>

<h2 id="manage">Manage your forms</h2>
<p>In the studio you don't manage your forms in a simple list. We created a tile system that gives you more control over the organization of your forms. There are multiple levels available to arrange your forms.</p>

<h3 id="workspaces">Workspaces</h3>
<p>Workspaces can be seen as pages inside your structure. You can use them to bundle collections and forms that belong together. You can create unlimited amount of workspaces. Your account always has one workspace</p>
<p>To create a new workspace, click the <code><i class="fas fa-plus"></i></code> icon on the top right side of a collection<i class="fas fa-arrow-right"></i>Click <code>Workspace</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-new-workspace.png" alt="Screenshot of workspaces in Tripetto" />
  <figcaption>Add a new workspace inside our first collection.</figcaption>
</figure>

<h3 id="collections">Collections</h3>
<p>Collections can be used to categorize forms to bundle corresponding forms. The system is so flexible you also can create workspaces inside collections for more levels, depending on the structure you want.</p>
<p>To create a new collection, click the <code><i class="fas fa-plus"></i></code> icon on the right side in a workspace.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-new-collection.png" alt="Screenshot of collections in Tripetto" />
  <figcaption>Add a new collection inside our first workspace.</figcaption>
</figure>

<h3 id="forms">Forms</h3>
<p>Inside a collection we can create an unlimited amount of forms.</p>
<p>To create a new form, click the <code><i class="fas fa-plus"></i></code> icon on the top right side of a collection<i class="fas fa-arrow-right"></i>Click <code>Conversational form</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-new-form.png" alt="Screenshot of forms in Tripetto" />
  <figcaption>Add a new form inside our second collection.</figcaption>
</figure>

<h3 id="form-name">Form names</h3>
<p>To keep track of your forms, it's recommended to give each form a good name, so you can find it back later on. To set the name of the form use the following options:</p>
<ul>
  <li>Open the form in the form builder<i class="fas fa-arrow-right"></i>Click the name of the form in the top left corner (<code>Unnamed</code> by default)<i class="fas fa-arrow-right"></i>Name your form;</li>
  <li>Open the form in the form builder<i class="fas fa-arrow-right"></i>Click <code>Customize</code><i class="fas fa-arrow-right"></i>Click <code>Properties</code><i class="fas fa-arrow-right"></i>Name your form.</li>
</ul>
<hr />

<blockquote>
  <h2 id="example">Example</h2>
  <p>Let's suppose we're a survey agency that creates surveys for multiple clients. We're totally free on how to organize this, but a possible structure could be the following:</p>
  <p>We can create a workspace for each client. In that way all forms of each client are separated.</p>
  <p>In each workspace (the client) we can create collections to organize the forms of that client. Let's say we want to organize all forms per year. So we can create a collection for each year.</p>
  <p>Inside each collection (the year) we create the forms of that particular year.</p>
  <p>So we end up with something like this:</p>
  <p><b>Workspace<i class="fas fa-arrow-right"></i>Collection<i class="fas fa-arrow-right"></i>Form</b></p>
  <p>Client A<i class="fas fa-arrow-right"></i>2019<i class="fas fa-arrow-right"></i>Form Z</p>
  <p>Client A<i class="fas fa-arrow-right"></i>2020<i class="fas fa-arrow-right"></i>Form Y</p>
  <p>Client A<i class="fas fa-arrow-right"></i>2020<i class="fas fa-arrow-right"></i>Form X</p>
  <p>Client B<i class="fas fa-arrow-right"></i>2020<i class="fas fa-arrow-right"></i>Form W</p>
  <p>Client C<i class="fas fa-arrow-right"></i>2018<i class="fas fa-arrow-right"></i>Form V</p>
  <figure>
    <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-example.png" alt="Screenshot of workspaces, collections and forms in Tripetto" />
    <figcaption>The workspace of Client A with two collections and three forms.</figcaption>
  </figure>
</blockquote>
