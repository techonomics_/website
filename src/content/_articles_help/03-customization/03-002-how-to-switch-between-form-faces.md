---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-switch-between-form-faces/
title: How to switch between form faces - Tripetto Help Center
description: One of the coolest things in Tripetto is you can switch each form between three different form faces. And you can switch between those all the time.
article_title: How to switch between form faces
article_id: form_faces
article_folder: customize
author: mark
time: 3
category_id: customization
subcategory: customization_faces
areas: [studio, wordpress]
popular: true
---
<p>One of the coolest things in Tripetto is you can switch each form between three different <strong>form faces</strong>. You can determine which form face helps you best to reach your goals.</p>

<h2 id="about">Why form faces?</h2>
<p>Maybe for one use case a certain form face will get you a higher completion rate, but for another use case another form face could work better. That's why you're totally free to determine what works best for each form that you're building.</p>
<p>Form faces totally change the way your form gets presented to your audience. It effects the whole experience you give to your respondents while filling out your form.</p>

<h2 id="switch">Switch your form face</h2>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-switch.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Switch your form face.</figcaption>
</figure>
<p>At the top menu bar of the form builder click <code><i class="fas fa-magic"></i> Customize</code>, or use the dropdown at the top of the preview. You can now choose between the different form faces. Just try them out while building your form:</p>
<h3 id="autoscroll">Autoscroll form face</h3>
<p>The autoscroll form face presents your questions one-by-one, giving it a modern feeling and maximized attention to each individual question. You really feel that form is smart!<br/><i>Perfect to create surveys, like customer satisfaction surveys.</i></p>
<figure>
  <img src="{{ page.base }}images/scenes/autoscroll-customer-satisfaction.png" alt="Screenshot of a autoscroll form face in Tripetto" class="no-shadow" />
  <figcaption>Example of a customer satisfaction survey in the autoscroll form face.</figcaption>
</figure>
<hr />

<h3 id="chat">Chat form face</h3>
<p>The chat form face presents your questions in a real chat interface, making it feel like a real online conversation.<br/><i>Perfect to create chatbots, like support interactions.</i></p>
<figure>
  <img src="{{ page.base }}images/scenes/chat-wedding-rsvp.png" alt="Screenshot of a chat form face in Tripetto" class="no-shadow" />
  <figcaption>Example of a wedding RSBP form in the chat form face.</figcaption>
</figure>
<hr />

<h3 id="classic">Classic form face</h3>
<p>The classic form face presents your form in a more traditional way, with multiple questions at one time, but of course with all benefits of smart logic and a good way of styling.<br/><i>Perfect to create website forms, like contact forms.</i></p>
<figure>
  <img src="{{ page.base }}images/scenes/classic-restaurant-reservation.png" alt="Screenshot of a classic form face in Tripetto" class="no-shadow" />
  <figcaption>Example of a restaurant reservation form in the classic form face.</figcaption>
</figure>
<hr />

<p>The form face you select in the builder is the form face your respondents get to see and use. You don't have to worry about the structure of your form: everything you build in the form builder is available in all form faces. Even your styling is preserved while switching the form faces.</p>

<h2 id="preview">Live preview</h2>
<p>The <a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/" target="_blank">live preview in the form builder</a> will always update what you're doing, so when you switch a form face, you'll see the new face immediately in the live preview. Over there you can see if the selected form face suits your needs and helps you to reach your goals.</p>

<h2 id="options">Additional options</h2>
<p>Each form face has its own additional options to improve the finishing touch, like extra settings, extra styling options and/or extra translatable messages. Read the help articles below to see these additional options for each form face:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/discover-additional-options-for-autoscroll-form-face/" target="_blank">Additional options for the autoscroll form face</a>;</li>
  <li><a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/" target="_blank">Additional options for the chat form face</a>;</li>
  <li><a href="{{ page.base }}help/articles/discover-additional-options-for-classic-form-face/" target="_blank">Additional options for the classic form face</a>.</li>
</ul>
