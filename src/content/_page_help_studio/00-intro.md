---
base: ../../
---

<section class="help-intro block-first">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="breadcrumb-navigation">
          <a href="{{ page.base }}help/">Help Center</a>
          <i class="fas fa-chevron-right"></i>
          <div class="dropdown">
            <a role="button" class="dropdown-button active" id="helpBreadcrumbProducts" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">Studio<i class="fas fa-chevron-down"></i></a>
            <div class="dropdown-menu" aria-labelledby="helpBreadcrumbProducts">
              <a class="dropdown-item" href="{{ page.base }}help/studio/"><div>Tripetto Studio<small>at tripetto.app</small></div></a>
              <a class="dropdown-item" href="{{ page.base }}help/wordpress/"><div>Tripetto WP<small>WordPress plugin</small></div></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <h1>Help with the studio</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-xl-8">
        <form action="{{ page.base }}help/studio/search/" method="get" class="help-search form-inline">
          <label class="sr-only" for="search-input">Search help articles for the studio</label>
          <input type="text" name="q" class="form-input form-input-big" id="search-input" placeholder="Search help articles for the studio" />
          <button class="button button-big"><i class="fas fa-search"></i><span>Search</span></button>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
