---
base: ../
---

<section class="launch-platforms">
  <div class="container">
    <div class="row launch-platform-images">
      <div class="col col-lg-6">
        <div class="row platform-holder launch-platform-left">
          <div class="col-xl-11">
            <img src="{{ page.base }}images/launch/studio.svg" alt="Illustration representing the studio" />
          </div>
        </div>
      </div>
      <div class="col col-lg-6">
        <div class="row platform-holder launch-platform-right">
          <div class="col-xl-11">
            <img src="{{ page.base }}images/launch/wordpress.svg" alt="Illustration representing the WordPress plugin" />
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="row platform-holder launch-platform-center">
          <div class="col-xl-11">
            <div class="platform palette-studio">
              <h2>Studio</h2>
              <p>Tripetto as web app</p>
              <ul>
                <li><h3>For SaaS users</h3></li>
                <li><h3>Available at tripetto.app</h3></li>
                <li><h3>Store data at Tripetto or self-host</h3></li>
                <li><h3>Free and pay-per-use (not recurring)</h3></li>
              </ul>
              <div class="platform-buttons">
                <a href="{{ site.url_app }}" target="_blank" class="button button-full">Start free, without account</a>
              </div>
            </div>
            <ul class="hyperlinks platform-hyperlinks">
              <li><a href="{{ page.base }}studio/" class="hyperlink hyperlink-small"><span>About the studio</span><i class="fas fa-arrow-right"></i></a></li>
              <li><a href="{{ page.base }}pricing/studio/" class="hyperlink hyperlink-small"><span>Studio pricing</span><i class="fas fa-arrow-right"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="row platform-holder launch-platform-center">
          <div class="col-xl-11">
            <div class="platform palette-wordpress launch-platform-hint">
              <h2>WP Plugin</h2>
              <p>Tripetto inside WordPress</p>
              <ul>
                <li><h3>For WordPress administrators</h3></li>
                <li><h3>Runs completely inside WordPress</h3></li>
                <li><h3>Store data inside the plugin only</h3></li>
                <li><h3>Free and paid subscriptions</h3></li>
              </ul>
              <div class="platform-buttons platform-buttons-multiple">
                  <a href="{{ site.url_wordpress_plugin }}" target="_blank" class="button button-full">Get free plugin</a>
                  <span class="button button-outline button-full button-wordpress-purchase" role="button">Buy premium</span>
              </div>
            </div>
            <ul class="hyperlinks platform-hyperlinks">
              <li><a href="{{ page.base }}wordpress/" class="hyperlink hyperlink-small"><span>About the WordPress plugin</span><i class="fas fa-arrow-right"></i></a></li>
              <li><a href="{{ page.base }}pricing/wordpress/" class="hyperlink hyperlink-small"><span>Plugin pricing</span><i class="fas fa-arrow-right"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="https://checkout.freemius.com/checkout.min.js"></script>
<script src="{{ page.base }}js/wordpress-freemius.js?v={{ site.cache_version }}"></script>
