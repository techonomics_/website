$(document).ready(function () {
    $("#carouselShowcase").carousel({
        interval: false,
        touch: false
    });

    $(".carousel-buttons li").on("click", function () {
        $("#carouselShowcase").carousel(parseInt($(this).data("slide-to")));
        $(".carousel-buttons li.active").removeClass("active");
        $(this).addClass("active");
        $("#carouselSwitcherLabel").text($(this).data('label'));
    });
});
