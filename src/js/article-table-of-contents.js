$(document).ready(function() {
    var tableOfContents = $("ul.article-table-of-contents");

    if (tableOfContents.length) {
        $(".article-content-core h2[id], .article-content-core h3[id]").each(function() {
            tableOfContents.append("<li class=\"level-" + ($(this).prop('nodeName').toLowerCase() == "h3" ? ("2") : "1") + "\"><a href=\"#" + this.id + "\">" + ($(this).data("anchor") ? $(this).data("anchor") : $(this).text()) + "</a></li>");
        });
    }
});
