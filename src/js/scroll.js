$(document).ready(function () {
    var $root = $("html, body");

    $(window).on('load', function(){
        if (window.location.hash !== "") {
            $root.animate({
                scrollTop: $(decodeURIComponent(window.location.hash)).offset().top-90
            }, 800);
        }
    });

    $("ul.anchors a,a.anchor, ul.article-table-of-contents a").on("click", function(event) {
        if (this.hash !== "") {
            event.preventDefault();

            $root.animate({
                scrollTop: $(decodeURIComponent(this.hash)).offset().top-90
            }, 800);
        }
    });
});
