$(document).ready(function () {
    $("#carouselPricingWordPress").carousel({
        interval: false,
        touch: false
    });

    $("#carouselPricingWordPress").on("slid.bs.carousel", function () {
        $(".carousel-buttons li.active").removeClass("active");
        $(".carousel-buttons li[data-slide-to=" + $("div.carousel-item.active").index() + "]").addClass("active");
    });

    $(".carousel-buttons li").on("click", function () {
        $("#carouselPricingWordPress").carousel(parseInt($(this).data("slide-to")));
        $(".carousel-buttons li.active").removeClass("active");
        $(this).addClass("active");
    });

    $(".switch-multi").on("click", function () {
        $("#carouselPricingWordPressMulti").click();
    });

    $(".switch-unlimited").on("click", function () {
        $("#carouselPricingWordPressUnlimited").click();
    });
});
