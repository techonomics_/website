(function () {
	function getQueryVariable(variable) {
		var query = window.location.search.substring(1),
			vars = query.split("&");

		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split("=");

			if (pair[0] === variable) {
				return pair[1];
			}
		}
	}

	function getPreview(query, content, previewLength) {
		previewLength = previewLength || (content.length * 2);

		var parts = query.split(" "),
			match = content.toLowerCase().indexOf(query.toLowerCase()),
			matchLength = query.length,
			preview;

		// Find a relevant location in content
		for (var i = 0; i < parts.length; i++) {
			if (match >= 0) {
				break;
			}

			match = content.toLowerCase().indexOf(parts[i].toLowerCase());
			matchLength = parts[i].length;
		}

		// Create preview
		if (match >= 0) {
			var start = match - (previewLength / 2),
				end = start > 0 ? match + matchLength + (previewLength / 2) : previewLength;

			preview = content.substring(start, end).trim();

			if (start > 0) {
				preview = "..." + preview;
			}

			if (end < content.length) {
				preview = preview + "...";
			}

			// Highlight query parts
			preview = preview.replace(new RegExp("(" + parts.join("|") + ")", "gi"), "<strong>$1</strong>");
		} else {
			// Use start of content if no match found
			preview = content.substring(0, previewLength).trim() + (content.length > previewLength ? "..." : "");
		}

		return preview;
	}

	function displaySearchResults(results, query) {
		var searchResultsEl = document.getElementById("search-results"),
			searchProcessEl = document.getElementById("search-process");

		if (query != "" && results.length) {
            var resultsHTML = "";
            var help_area = window.localStorage.getItem("tripetto_help_area");
            var sArea = "";

            if (help_area == "studio" || help_area == "wordpress" || help_area == "sdk") {
                switch (help_area) {
                    case "studio":
                        sArea = "Studio";
                        break;
                    case "wordpress":
                        sArea = "WordPress";
                        break;
                    case "sdk":
                        sArea = "SDK";
                        break;
                }
            }

			results.forEach(function (result) {
				var item = window.data[result.ref],
					contentPreview = getPreview(query, item.content, 150),
					titlePreview = getPreview(query, item.title);

                    resultsHTML += "<li>";
                    resultsHTML += "<div class=\"palette-" + item.category_id + "\" onclick=\"window.location='" + item.url + "';\">";
					resultsHTML += "<a href=\"" + item.url + "\"><h3>" + titlePreview + "</h3></a>";
					resultsHTML += "<p>" + contentPreview + "</p>";
					resultsHTML += "<small class=\"pills\"><span class=\"palette-" + item.category_id + " pill-splitter pill-splitter-after\">"+ item.category_pill +"</span>"+ (item.time > 0 ? ("<span><i class=\"fas fa-file-alt\"></i>" + item.time + " Min.</span>") : "") + (item.time_video > 0 ? ("<span><i class=\"fas fa-file-video\"></i>" + item.time_video + " Min.</span>") : "") + "</small>";
                    resultsHTML += "</div>";
                    resultsHTML += "</li>";
			});

			searchResultsEl.innerHTML = resultsHTML;
			searchProcessEl.innerText = results.length;
		} else {
			searchResultsEl.style.display = "none";
			searchProcessEl.innerText = "No";
		}
	}

	var index = lunr(function () {
        this.b(0);
        this.ref("id");
		this.field("title", {boost: 2});
		this.field("category");
		this.field("url");
		this.field("content", {boost: 4});

		for (var key in window.data) {
			this.add(window.data[key]);
		}
    });

	var query = decodeURIComponent((getQueryVariable("q") || "").replace(/\+/g, "%20")),
		searchQueryContainerEl = document.getElementById("search-query-container"),
		searchQueryEl = document.getElementById("search-query"),
		searchInputEl = document.getElementById("search-input");

	searchInputEl.value = query;
	searchQueryEl.innerText = query;
    searchQueryContainerEl.style.display = "inline";

    displaySearchResults(index.search(query), query);
})();
