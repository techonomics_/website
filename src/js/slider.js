function showcaseSlider(id) {
    var sliderContainer = document.querySelector(id);
    var before = sliderContainer.querySelector(".slider-before");
    var beforeText = sliderContainer.querySelector(".slider-before-position");
    var afterText = sliderContainer.querySelector(".slider-after-position");
    var handle = sliderContainer.querySelector(".slider-handle");
    var widthChange = 0;

    before.setAttribute("style", "width: 30%;");
    handle.setAttribute("style", "left: 30%;");

    sliderContainer.addEventListener("mousemove", function (e) {
        var containerWidth = sliderContainer.offsetWidth;
        widthChange = e.offsetX;
        var newWidth = (widthChange * 100) / containerWidth;

        if (e.offsetX > 10 && e.offsetX < sliderContainer.offsetWidth - 10) {
            before.setAttribute("style", "width:" + newWidth + "%;");
            afterText.setAttribute("style", "z-index:" + "1;");
            handle.setAttribute("style", "left:" + newWidth + "%;");
        }
    });

    sliderContainer.addEventListener("touchstart", function (e) {
        sliderContainer.addEventListener("touchmove", function (e2) {
            var containerWidth = sliderContainer.offsetWidth;
            var currentPoint = e2.changedTouches[0].clientX;
            var startOfDiv = sliderContainer.offsetLeft;
            var modifiedCurrentPoint = currentPoint - startOfDiv;

            if (modifiedCurrentPoint > 10 && modifiedCurrentPoint < sliderContainer.offsetWidth - 10) {
                var newWidth = (modifiedCurrentPoint * 100) / containerWidth;
                before.setAttribute("style", "width:" + newWidth + "%;");
                afterText.setAttribute("style", "z-index: 1;");
                handle.setAttribute("style", "left:" + newWidth + "%;");
            }
        });
    });
};

showcaseSlider("#showcaseAutoscroll");
showcaseSlider("#showcaseChat");
showcaseSlider("#showcaseClassic");
