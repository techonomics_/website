$(document).ready(function () {
    var articleProgress = $("#articleProgress");

    if (articleProgress.length) {
        var fnProgress = function () {
            requestAnimationFrame(function () {
                if ($(this).scrollTop() > 60) {
                    articleProgress.addClass("progress-show");
                } else {
                    articleProgress.removeClass("progress-show");
                }

                var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
                var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
                var scrolled = (winScroll / height) * 100;
                $("#articleProgressBar").css("width", scrolled + "%");
            });
        };

        $(document).scroll(fnProgress);
        $(document).ready(fnProgress);
    }
});
