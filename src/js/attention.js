$(document).ready(function () {
    var attention = $(".attention");

    if (attention.length) {
        var fnScrollAttention = function () {
            requestAnimationFrame(function () {
                if ($(this).scrollTop() > 600) {
                    attention.addClass("attention-out");
                } else {
                    attention.removeClass("attention-out");
                }
            });
        };

        $(document).scroll(fnScrollAttention);
        $(document).ready(fnScrollAttention);
    }
});
