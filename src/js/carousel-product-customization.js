$(document).ready(function () {
    $("#carouselProductCustomization").carousel({
        interval: false,
        touch: false
    });

    $("#carouselProductCustomization").on("slid.bs.carousel", function () {
        $(".carousel-buttons li.active").removeClass("active");
        $(".carousel-buttons li[data-slide-to=" + $("div.carousel-item.active").index() + "]").addClass("active");
    });

    $(".carousel-buttons li").on("click", function () {
        $("#carouselProductCustomization").carousel(parseInt($(this).data("slide-to")));
        $(".carousel-buttons li.active").removeClass("active");
        $(this).addClass("active");
    });
});
